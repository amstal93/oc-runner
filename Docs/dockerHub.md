
## DockerHub API

### Common options for DockerHub API

- `--image=namespace/image` need to be a valid docker image `repo-name`:
    - valid:
        - `centos`
        - `library/centos`
        - `register.hub.docker.com/library/centos`
    - tag is ignored, thus follow examples also are valid:
        - `centos:latest`
        - `library/centos:7.4`
        - `register.hub.docker.com/library/centos:7.5`
- `--credentials=$(base64(username:password))`
    - Create base64 string credentials with a docker given `username` and `password`:

    ```bash
echo -e -n "username:password" | base64 -
```
    - verify with:

    ```bash
echo -e -n "dXNlcm5hbWU6cGFzc3dvcmQ=" | base64 -D  -
```
    Should return "username:password"

    - Or extract from docker config,(beyond this scope, google is your friend)

- optional [`--allow-fail`] will generate only a `Warning` in stead of failing the build.


### Set OR Create Docker Hub META data


```bash
DockerHub_set_description --image=${repo-name} \
                         [--credentials=$(base64(username:password))] \
                         [--set-private[=yes|no|auto|none]] \
                         [--full_description="my DockerHub long description"] \
                         [--description="my description"] \
                         [--allow-fail]
```

*Defaults:*

- `[--credentials=${DOCKER_CREDENTIALS}]`
- `[--full_description=${OC_INFO_IMAGE_DESCRIPTION} | file:./description.md | file:./README.md]`
- `[--description=${OC_INFO_IMAGE_TITLE} | ${OC_INFO_IMAGE_REFNAME} | "reverse of repository namespace" ]`
- `[--set-private=auto]`

    1.  If the docker repository does not exist, it will be created!
    2.  DockerHub_set_description allows to set the repository private:
        - default to `auto`, meaning depending on visibility of the Gitlab project:
            - if the project visibility is `public` then the docker repository is set to `public`
            - if the project visibility is `private` or `internal` then the docker repository is set to `private`
        - If `--set-private` option is **not** set or is set to a value other then `yes` or `no` or `none` it default to `auto`
		- if `--set-private` or `--private=yes` the Docker repository is set to **private**.
		- if `--set-private=no` the Docker repository is set **public**.
                - if `--set-private=none` the Docker repository private setting is **not altered**!


    **note**

    - If the repository for the given image does not exists, it will be created, this allows to create a private repository before a registry_push, ensuring that a private image stays private!
    - The Project visibility of Gitlab is only available since Gitlab version `10.3` in CI, before `v10.3`, the `--set-private=auto` option will result in a public docker Repository.


### Delete a Docker hub Tag

```bash
DockerHub_delete_tag \
                     [--allow-fail] \
                     --image=${image} \
                     --reference=${tag} \
                     [--credentials=$(base64(username:password))]
```
Deletes a given tag for a given repository image.

### Delete a Docker hub repository

```bash
DockerHub_delete_repository \
                            [--allow-fail] \
                            --image=${image} \
                            [--credentials=$(base64(username:password))]
```

Deletes a repository with given image name.

**Warning** this deletes the repository with all tags and description on the DockerHub! (carefull!!!)

[top](#oc-runner)

