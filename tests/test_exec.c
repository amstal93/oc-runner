/*
 test_exec.c
 Created by Danny Goossen, Gioxa Ltd on 26/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


#include <check.h>
#include "../src/deployd.h"


char * user;
char * group;

uid_t thisuid;
gid_t thisgid;
static void setup(void)
{
  struct passwd *pw;
   struct group * grp;

   thisuid = geteuid();
   thisgid= getgid();
   grp = getgrgid(thisgid);
   pw = getpwuid (thisuid);
   user=pw->pw_name;
  group=grp->gr_name;
}



START_TEST(check_guid)
{
  uid_t uid;
  gid_t gid;
  get_gid(&gid,user);
  get_uid(&uid,user);
  ck_assert_int_eq(uid,thisuid);
  ck_assert_int_eq(gid,thisgid);
  ck_assert_int_ne(get_gid(&gid,"fakeusername"),0);
  ck_assert_int_ne(get_uid(&uid,"fakeusername"),0);
}
END_TEST

START_TEST(check_exec)
{
  uid_t uid;
  gid_t gid;
  get_gid(&gid,user);
  get_uid(&uid,user);
  data_exchange_t data_exchange;
  void * opaque=&data_exchange;
  int exitcode=0;
  char * newarg[4];
  cJSON * env_json=NULL;
   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);

  data_exchange.needenvp=0;
  data_exchange.gid=gid;
  data_exchange.uid=uid;
  data_exchange.env_json=env_json;
  data_exchange.paramlist=(char **)newarg;
  data_exchange.timeout=1;
  data_exchange.trace=trace;
  data_exchange.current_user=1;

  newarg[0]="/usr/bin/sh";
  newarg[1]="-c";

  newarg[3]=NULL;

  char this_command[256];
  data_exchange.this_command=this_command;


  sprintf(this_command,"exit OK");
   clear_dynamic_trace(trace);
  newarg[2]="[ 1 == 1 ]";
  exitcode= cmd_exec(opaque);
  alert("%s\n",get_dynamic_trace(trace));
  ck_assert_int_eq(exitcode,0);
  alert("%s\n",get_dynamic_trace(trace));
  //ck_assert_int_eq(0,strlen(get_dynamic_trace(trace)));

  sprintf(this_command,"exit fail");
   clear_dynamic_trace(trace);
  newarg[2]="[ 1 = 0 ]";
  exitcode= cmd_exec(opaque);
  ck_assert_int_ne(exitcode,0);
  alert("%s\n",get_dynamic_trace(trace));
  //ck_assert_int_eq(0,strlen(get_dynamic_trace(trace)));
  sprintf(this_command,"exit OK, stdout feedback");
   clear_dynamic_trace(trace);
  newarg[2]="echo test";
  exitcode= cmd_exec(opaque);
  ck_assert_int_eq(exitcode,0);
  alert("%s\n",get_dynamic_trace(trace));
  //ck_assert_int_eq(5,strlen(get_dynamic_trace(trace)));

  sprintf(this_command,"exit NOK, timeout");
   clear_dynamic_trace(trace);
  newarg[2]="sleep 2";
  exitcode= cmd_exec(opaque);
  ck_assert_int_ne(exitcode,0);


  sprintf(this_command,"exit OK, set envp");
   clear_dynamic_trace(trace);
  data_exchange.needenvp=1;
  env_json=cJSON_CreateObject();
  cJSON_AddItemToObject(env_json,"myenv_var" , cJSON_CreateString("testenvp"));
  
  newarg[2]="echo $myenv_var";
  exitcode= cmd_exec(opaque);
  ck_assert_int_eq(exitcode,0);
  cJSON_Delete(env_json);



  // one more a signal?
  // kill -30 %1
  #ifndef __APPLE__
  sprintf(this_command,"signal");
   clear_dynamic_trace(trace);
  data_exchange.needenvp=0;

  newarg[2]="kill -9 $BASHPID";
  exitcode= cmd_exec(opaque);
  printf("signal response >%s<\n",get_dynamic_trace(trace));
  ck_assert_int_ne(exitcode,0);
  #endif
  //output_buf[strlen("testenvp")]=0; // avoid the real non escaped "\n"
  //ck_assert_str_eq(output_buf,"testenvp");
   free_dynamic_trace(&trace);

}
END_TEST

START_TEST(check_writefile)
{
  uid_t uid;
  gid_t gid;
  get_gid(&gid,user);
  get_uid(&uid,user);
  data_exchange_t data_exchange;
  void * opaque=&data_exchange;

  int exitcode=0;
  char * newarg[11];
  cJSON * env_json=NULL;
  char this_command[256];
   _mkdir("test_exec_check_writefile","") ;//, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  setdebug();
  setverbose();

 
   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);
  data_exchange.needenvp=0;
  data_exchange.gid=gid;
  data_exchange.uid=uid;
  data_exchange.env_json=NULL;

  data_exchange.paramlist=(char **)newarg;
  data_exchange.timeout=3;
  data_exchange.trace=trace;
  data_exchange.this_command=this_command;

  newarg[0]="write test file";
  newarg[1]="test_exec_check_writefile/writetest.txt";
  newarg[2]="writewtest OK";
  newarg[3]=NULL;
   clear_dynamic_trace(trace);
  sprintf(this_command,"write file");
  exitcode=cmd_write(opaque);
  ck_assert_int_eq(exitcode,0);
 // cleanup test file
  newarg[0]="rm";
  newarg[1]="-f";
  newarg[2]="test_exec_check_writefile/writetest.txt";
  newarg[3]=NULL;
   clear_dynamic_trace(trace);
  sprintf(this_command,"cleanup");
  exitcode=cmd_exec(opaque);

  newarg[0]="write test file";
  newarg[1]="unknowndir/writetest.txt";
  newarg[2]="writewtest OK";
  newarg[3]=NULL;

   clear_dynamic_trace(trace);
  sprintf(this_command,"write file to bad dir");
  exitcode=cmd_write(opaque);
  ck_assert_int_ne(exitcode,0);
   remove("test_exec_check_writefile");

   free_dynamic_trace(&trace);

}
END_TEST


Suite * exec_suite(void)
{
   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_exec");
   /* Core test case */
   tc_core = tcase_create("Core");
   tcase_add_checked_fixture(tc_core, setup,NULL);
   //tcase_add_unchecked_fixture(tc_core, setup, teardown);
   tcase_set_timeout(tc_core,65);
   tcase_add_test(tc_core, check_guid);
   tcase_add_test(tc_core, check_exec);
   tcase_add_test(tc_core, check_writefile);
   suite_add_tcase(s, tc_core);
   return s;
}


int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;

   s = exec_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_VERBOSE | CK_NOFORK);
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
