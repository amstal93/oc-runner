//
//  test_coders.c
//  oc-runner
//
//  Created by Danny Goossen on 16/9/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

//LCOV_EXCL_START
#include <check.h>
//LCOV_EXCL_STOP

#include "../src/b64.h"
#include "../src/openssl.h"

struct test_data_b64_t
{
	char * in;
	int len;
	char * out;
} ;

static const struct test_data_b64_t testlist_b64[3] = {
	{ NULL,0,""},
	{ "",0,""},
	{ "a*_/+?-%^&()!@#=[|/?.<,.09zZ",28,"YSpfLys/LSVeJigpIUAjPVt8Lz8uPCwuMDl6Wg=="}
};

START_TEST(check_b64)
{
	char * result=NULL;
	size_t len_out=0;
	base64_encode((uint8_t *)testlist_b64[_i].in, testlist_b64[_i].len, &result, &len_out, NULL);
	ck_assert_str_eq(result, testlist_b64[_i].out);
	unsigned char * dec=NULL;
	size_t dec_len=0;
	base64_decode(result, len_out, &dec, &dec_len, NULL);
	if (testlist_b64[_i].in ==NULL || testlist_b64[_i].len ==0)
		ck_assert_int_eq(dec_len, 0);
	else
		ck_assert_str_eq( testlist_b64[_i].in,(char *)dec);
	if (dec) free(dec);
	if(result) free(result);
}
END_TEST


struct test_data_b64_v_t
{
	char * prefix;
	char * format;
	char * in;
	char * out;
} ;

static const struct test_data_b64_v_t testlist_b64_v[4] = {
	{ NULL,"%s","",""},
	{ "",NULL,NULL,NULL},
	{ NULL,"%s","a*_/+?-%^&()!@#=[|/?.<,.09zZ","YSpfLys/LSVeJigpIUAjPVt8Lz8uPCwuMDl6Wg=="},
	{ "pref : ","%s","a*_/+?-%^&()!@#=[|/?.<,.09zZ","pref : YSpfLys/LSVeJigpIUAjPVt8Lz8uPCwuMDl6Wg=="}
};

START_TEST(check_b64_v)
{
	char * result=Base64Encode_v(testlist_b64_v[_i].prefix, testlist_b64_v[_i].format,testlist_b64_v[_i].in);
	
	if (result!=NULL)
		ck_assert_str_eq(result, testlist_b64_v[_i].out);
	else
		ck_assert_ptr_null(testlist_b64_v[_i].out);
	
	if (result) free(result);
	
}
END_TEST


Suite * coders_suite(void)
{
	Suite *s;
	TCase *tc_core;
	//TCase *tc_progress;
	s = suite_create("test_error");
	/* Core test case */
	tc_core = tcase_create("Core");
	//tcase_add_checked_fixture(tc_core, setup, teardown);

	tcase_add_loop_test (tc_core,check_b64,0,3);
	tcase_add_loop_test (tc_core,check_b64_v,0,4);
	tcase_add_unchecked_fixture(tc_core, NULL, NULL);
	tcase_set_timeout(tc_core,15);
	suite_add_tcase(s, tc_core);
	return s;
}


int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;
	
	s = coders_suite();
	sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

