#
#  Makefile.am
#  deployctl /scripts
#
# Created by Danny Goossen on 2/3/16.
#
# MIT License
#
# Copyright (c) 2017 deployctl, Gioxa Ltd.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

DEPLOYD=../src/deployd
DEPLOYDAEMON=$(DEPLOYD) --daemon --current_usr
DEPLOYCTL=../src/deployctl

C_TESTS = \
   test_error \
   test_specials \
   test_utils \
   test_coders


TESTS= $(C_TESTS)

CLEANFILES= COVERAGE coverage.url

check_PROGRAMS= $(C_TESTS)

test_error_SOURCES= test_error.c
test_error_LDADD=  ../src/error.o


test_dyn_buffer_SOURCES= test_dyn_buffer.c
test_dyn_buffer_LDADD=../src/dyn_buffer.o ../src/error.o ../src/cJSON_deploy.o ../src/exec.o ../src/utils.o ../src/yaml2cjson.o

test_cJSON_deploy_SOURCES= test_cJSON_deploy.c utils.c
test_cJSON_deploy_LDADD=    ../src/dyn_buffer.o \
                            ../src/error.o \
                            ../src/cJSON_deploy.o \
                            ../src/yaml2cjson.o \
                            ../src/utils.o

test_yaml_SOURCES= test_yaml.c
test_yaml_LDADD=../src/dyn_buffer.o \
                      ../src/error.o \
                      ../src/cJSON_deploy.o \
                      ../src/exec.o \
                      ../src/utils.o \
                      ../src/yaml2cjson.o

test_exec_SOURCES= test_exec.c
test_exec_LDADD=   ../src/dyn_buffer.o \
                   ../src/error.o \
                   ../src/cJSON_deploy.o \
                   ../src/yaml2cjson.o \
                   ../src/utils.o \
                   ../src/exec.o

test_specials_SOURCES= test_specials.c
test_specials_LDADD=   ../src/dyn_buffer.o \
                       ../src/error.o \
                       ../src/cJSON_deploy.o \
                       ../src/wordexp_var.o \
                       ../src/specials.o \
                       ../src/yaml2cjson.o \
                       ../src/utils.o

test_utils_SOURCES= test_utils.c
test_utils_LDADD=   ../src/dyn_buffer.o \
                    ../src/error.o \
                    ../src/cJSON_deploy.o \
                    ../src/utils.o

test_coders_SOURCES= test_coders.c
test_coders_LDADD= ../src/dyn_buffer.o \
                   ../src/error.o \
                   ../src/cJSON_deploy.o \
                   ../src/openssl.o \
                   ../src/b64.o \
                   ../src/jose_error.o \
                   ../src/utils.o


EXTRA_DIST= test_web_api.h utils.h

# add include flags from source

AM_CPPFLAGS = -I$(top_srcdir)/src/

SOURCES = $(top_builddir)/src/deployd \
          $(top_builddir)/src/deployctl

AM_CFLAGS=-D GITVERSION=\"@GIT_VERSION@\" $(CHECK_CFLAGS) $(CODE_COVERAGE_CFLAGS) -D_GNU_SOURCE
AM_LDFLAGS= $(CHECK_LIBS) $(CODE_COVERAGE_LDFLAGS)

#LDADD= $(CHECK_LIBS)

@CODE_COVERAGE_RULES@
@VALGRIND_CHECK_RULES@
#     VALGRIND_SUPPRESSIONS_FILES = my-project.supp
#     EXTRA_DIST = my-project.supp
