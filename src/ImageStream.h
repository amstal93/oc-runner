/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/**
 * \file ImageStream.h
 * \brief headers oc-runner commands openshift ImageStream api
 * \author Created by Danny Goossen on  9/8/18.
 * \copyright 2018, Danny Goossen. MIT License
 */

#ifndef __oc_runner__ImageStream__
#define __oc_runner__ImageStream__

#include <stdio.h>
#include "cJSON_deploy.h"
#include "dyn_trace.h"
#include "image_parameters.h"

/**
 \brief  Delete a image-tag from ImageStream registry
 
 Delete on existing image tag from IS (oc-api delete imagetag)
 
 Command line options:
 
 
 --name=<name>
 --reference=<reference>
 --allow_fail
 
 Defaults to: <reference>: latest
 
 \param job to execute
 \param trace the feedback structure for the build-trace in gitlab
 \param argc number of arguments
 \param argv the command line arguments from the build script
 \return 0 on success
 */
int ImageStream_delete_tag(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const*argv);

/**
 \brief  Delete an image from a cluster ImageStream registry
 
 Delete on existing image from IS (oc-api delete image)
 
 Command line options:
 --name=<name>
 --reference=<reference>
 --allow_fail
 
 Defaults to:
 <reference>: latest
 
 
 \param job to execute
 \param trace the feedback structure for the build-trace in gitlab
 \param argc number of arguments
 \param argv the command line arguments from the build script
 \return 0 on success
 */
int ImageStream_delete(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const*argv);

// int rootfs_2_layer(struct trace_Struct *trace ,const char * image_dir, const char * rootfs_dir,char digest_tar[65+7],char digest_tar_gz[65+7],int *layer_size,const char *layername);




/**
 *  \brief Creates an empty Imagestream map, in case EC2 node, which cannot push a manifest
 *  \param namespace we operate in
 *  \param image_name of the imagestream
 *  \return the Imagestream_map in cjson struct or NULL on failure
 *  \note caller is responsible to free the struct
 */
cJSON* make_imagestream_template( const char * namespace, const char * image_name);



#endif /* defined(__oc_runner__ImageStream__) */
