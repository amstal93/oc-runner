//
//  transfer_blobs.c
//  oc-runner
//
//  Created by Danny Goossen on 29/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

/** \file transfer_blobs.c
 *  \brief Code for parallel transfering blobs
 *  \author Created by Danny Goossen on 29/1/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 */

#include "transfer_blobs.h"
#include "deployd.h"

#include "dkr_api.h"
#include "curl_headers.h"

#define RETRY_HTTP(X) ({ int res=0; int count=0;do{ if (count==0) usleep(2000000LL*count*count); res=X; count++; }while((res<100 || res>=500) && count<6);res;})

/*!
 * \brief Maximun number of threads used for parallel artifacts downloads
 */
#define NUMT 6

/**
 \brief Data struct for commincatiog a blob transfer
 */
struct dl_blob_thread_s {
   int thread_Number;                  /**< this thread Number*/
   char ** from_image;                 /**< from image (registry) */
   char ** to_image;                   /**< to image (registry) */
   char * transfer_buffer;             /**< the buffer for transfering data*/
   pthread_mutex_t buffer_lock;      /**< lock for updating/retrieving /a transfer_buffer */
   double lastruntime;                 /**< last time we had a progress callback */
   int free;                           /**< thread free if 1 */
   int finish;                         /**< transfer finished if 1*/
   int error;                          /**< transfer error indicator */
   pthread_t tid[2];                   /**< thread pid's of up and down */
   cJSON * blob;                       /**< this blob */
   CURL  *curl_up;                     /**< curl handle upload a blob */
   CURL  *curl_down;                   /**< curl handle download a blob */
   char * url_up;                      /**< upload blob url*/
   char * url_down;                    /**< download blob url */
   char * Auth_up;                     /**< www-auth for upload blob */
   char * Auth_down;                   /**< www-auth for download blob */
   int progress;                       /**< boolean indicating that there was progress */
   int error_count;                    /**< number of times error for the given transfer*/
   pthread_cond_t s_buff_is_empty;     /**< conditional signal data in \a #transfer_buffer is cleared*/
   pthread_cond_t s_buff_is_not_empty; /**< conditional signal data in \a #transfer_buffer */
   int Buff_empty;                     /**< indicate if \a transfer_buffer is empty */
   size_t Buff_size;                   /**< chunksize of data to transfer */
   int Size_to_transfer;           /**< size of blob to transfer -1 if not known */
	size_t size_received;
	size_t size_send;
	struct curl_headers_struct headers_push;
	struct curl_headers_struct headers_pull;
};

/**
 \brief Main data Structure for Transfer Blobs
 
 */
struct dl_blobs_transfer_s {
   char * from_image;                    /**< from image (registry) */
   char * to_image;                      /**< to image (registry) */
   struct dl_blob_thread_s data[NUMT];   /**< Array of thread data structures, #dl_blob_thread_s one for each thread */
   int free_count;                       /**< Number of free threads (\b NUMT - used threads )*/
   int todo;                             /**< How many downloads sill to do */
   int next;                             /**< next item to download, -1 if finished */
   cJSON * blobs_list;                   /**< list of blobs to transfer, with status */
   struct dkr_api_data_s * org_api_data; /**< docker registry api handle */
};


// Level 1 private Forward declarations for Main transfer

/**
 *  @brief Initialize the artifacts downloads
 *  init and have a struct pointer with thread info
 *  @param dl_blobs_transfer provide with clean struct
 *  @param from_image source image name
 *  @param to_image destination image
 *  @param api_data Ptr to dkr api data handle, to be used for authentification.
 *
 *  @note Use close_transfer_blobs() to free memory
 */
static void init_blobs_transfer(struct dl_blobs_transfer_s * dl_blobs_transfer, char * from_image, char * to_image, cJSON * bloblist, struct dkr_api_data_s * api_data );


/*!
 @brief scan blobs transfer status
 quick scan of the threads to see if ANY PROGRESS CHANGE
 resturns 1 on an update
 @param blobs_list blob data list
 @param trace the job trace
 */
static void scan_blobs_status(cJSON * blobs_list, struct trace_Struct *trace,int final);


/**
 *  @brief free's all memory and cleans up
 *  Cleanup the stuct
 *  @param dl_blobs_transfer where all data is stored
 */
static void close_transfer_blobs(struct dl_blobs_transfer_s * dl_blobs_transfer);

/**
 *  @brief Cancels all downloads
 *
 *  @param dl_blobs_transfer where all data is stored
 */
static void cancel_transfer_blobs(struct dl_blobs_transfer_s * dl_blobs_transfer);

/*!
 *  @brief transfer the \a blobs
 *  This function is re-entrant called,
 *   when needed it will start new threads for a new blob transfer
 *
 *  @param dl_blobs where all data is stored for the transfers
 *  @return returns the number of actions to do, -1 on error
 *  @note Re-entrant fuction
 *
 */
static int transfer_blobs_iteration( struct dl_blobs_transfer_s * dl_blobs);

/*!
 *  @brief quick scan of the threads to see if ANY PROGRESS CHANGE
 *
 *  @param dl_blobs_transfer where all data is stored
 *  @return returns 1 if there was a change in progress
 *  @note used to see if we can update feedback to client
 */
static int progress_change_blobs(struct dl_blobs_transfer_s * dl_blobs_transfer);

/*!
 *  @brief find next blob
 *
 *  @param dl_blobs where all data is stored
 *  @return returns cjson struct of next blob
 */
static cJSON * find_next_blob(struct dl_blobs_transfer_s * dl_blobs);

// =========================
//  Main public function
// =========================


int transfer_blobs(cJSON * manifest, char * from_image, char * to_image, struct dkr_api_data_s * api_data, struct trace_Struct * trace)
{
   debug("\n***********************\n Transfer Blobs\n***********************\n\n");
   struct dl_blobs_transfer_s dl_blobs_transfer;
   init_blobs_transfer(&dl_blobs_transfer, from_image, to_image, manifest, api_data);
   int res=1;
   int update_error = 0;
   int res_prev=0;
   set_mark_dynamic_trace(trace);
   scan_blobs_status(dl_blobs_transfer.blobs_list,trace,0);
   int count_u=0;
   while ( res>0 && !update_error ) //todo  && !parameters->hubsignal && !parameters->exitsignal) // loop till finish
   {
      int progress_update=0;
      res=transfer_blobs_iteration(&dl_blobs_transfer);
      progress_update=progress_change_blobs(&dl_blobs_transfer);
      if ((res!=res_prev) || res <1 || progress_update) // resduce network trafic, only report changes
      {
         if ((count_u%5 ==0) || res <1 || progress_update)
         {
            scan_blobs_status(dl_blobs_transfer.blobs_list,trace,0);
            if (update_error){
               printf("\n** main update details error, canceling downloads\n");
               cancel_transfer_blobs(&dl_blobs_transfer);
            }
         }
         count_u ++;
      }
      res_prev=res;
   }
   scan_blobs_status(dl_blobs_transfer.blobs_list,trace,1);
   close_transfer_blobs(&dl_blobs_transfer);
   return res;
}


// ***************************
// level 2 private functions
// ***************************

/**
 *  @brief Start an transfer
 *  @param dl_blobs struct with all relevant info to do the transfer
 *  @param thread_number the number of this thread
 *  @return 0 on success
 */
static int start_a_blob_transfer(struct dl_blobs_transfer_s * dl_blobs,int thread_number);


/**
 *  @brief Create a bloblist from a manifest with given \a version
 *  @param manifest cJSON object of the manifest
 *  @param version manifest version
 *  @return cJSON bloblist
 *  @note caller is responsible for freeing the object
*/
static cJSON * create_bloblist(cJSON *manifest,int version);



// ********************************
// FORWARD static declarations
// level 3 private Functions Curl
// ********************************

/*!
 * @brief Push a blob, using \a cb_data_up and \a progress_transfer_up
 
 */
static void * push_blob(void * arg);

/*!
 * @brief Pull a blob using \a cb_data_down and \a cb_data_down
 
 */
static void * pull_blob(void * arg);


/*!
 * @brief curl callback Write data Function for download blob
 
 */
static size_t cb_data_down(void *contents, size_t size, size_t nmemb, void *userp);

/*!
 * @brief curl callback READ data Function for Upload blob
 
 */
static size_t cb_data_up(void *contents, size_t size, size_t nmemb, void *userp);


/*!
 * @brief curl callback progress upload blob
 
 */
static int progress_transfer_up(void *userp,  curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow);
/*!
 * @brief curl callback progress upload blob for curl version < 0x072000 (7.32)
 
 */
static int progress_transfer_up_old(void *userp,   double dltotal,   double dlnow,   double ultotal,   double ulnow);


/*!
 * @brief curl callback progress download blob
 
 */
static int progress_transfer_down(void *userp,  curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow);

/*!
 * @brief curl callback progress donload blob for curl version < 0x072000 (7.32)
 
 */
static int progress_transfer_down_old(void *userp,   double dltotal,   double dlnow,   double ultotal,   double ulnow);



/*!
 * @brief cancel upload
 
 */
static void cancel_upload(void * userp);

// ===========================
//         CODE Start
// ===========================

// ******************************************
// Level 1 Private Functions, called by Main
// ******************************************

void scan_blobs_status(cJSON * blobs_list, struct trace_Struct *trace,int final)
{
   debug("**** scan blob transfer status\n");
   cJSON * blob;
   //clear_till_mark_dynamic_trace(trace);
 
   cJSON_ArrayForEach(blob,blobs_list)
   {
	   const char* dp_name=cJSON_get_key(blob, "name");
	   if (dp_name)
		  Write_dyn_trace_pad(trace,none,22,"     + %s",dp_name);
	   else
		  Write_dyn_trace_pad(trace,none,22,"     + Image Layer");
      Write_dyn_trace_pad(trace,none,22,": %.*s",19,cJSON_get_key(blob, "sha256"));
      cJSON * blobsize_obj=cJSON_GetObjectItem(blob, "size");
      if (blobsize_obj)
      {
         char formatbuf[FORMAT_SIZE_BUF];
         Write_dyn_trace(trace, yellow, " [");
         Write_dyn_trace_pad(trace,yellow,6,"%s",format_size(formatbuf, blobsize_obj->valueint));
         Write_dyn_trace(trace, yellow, "]");
      }
      else
		  
         Write_dyn_trace(trace,none,"         ");
      char foul[]="          ";
      cJSON * error_count_obj=cJSON_GetObjectItem(blob, "error_count");
      if (error_count_obj && error_count_obj->valueint>1)
      {
         Write_dyn_trace(trace,yellow,"(%d)",error_count_obj->valueint-1);
         foul[error_count_obj->valueint]='\n';
         foul[error_count_obj->valueint+1]=0;
      }
      else
      {
         Write_dyn_trace(trace,magenta,"   ");
         foul[0]='\n';
         foul[1]=0;
      }
      dkr_tr_code status=cJSON_GetObjectItem(blob,"status")->valueint;
      if (status==DR_TR_STAT_OK)
         Write_dyn_trace(trace,green,      "      [OK]");
      else if (status==DR_TR_STAT_ERROR)
      {
         if (cJSON_GetObjectItem(blob, "error_count")->valueint<=3)
            Write_dyn_trace(trace,magenta, "   [ERROR]");
         else
            Write_dyn_trace(trace,red,     "    [FAIL]");
        // Write_dyn_trace_pad(trace, none,cJSON_GetObjectItem(blob, "error_count")->valueint , "    ");
      }
      else if (status==DR_TR_STAT_RETRY)
       Write_dyn_trace(trace,cyan,         "   [RETRY]");
      else if (status==DR_TR_STAT_PENDING)
        Write_dyn_trace(trace,yellow,      " [PENDING]");
      else if (status==DR_TR_STAT_NA)
         Write_dyn_trace(trace,green,      "    [skip]");
      else if (status==DR_TR_STAT_TRANSFER)
      {
         if (blobsize_obj)
         Write_dyn_trace(trace, blue,      "      %3d%%",cJSON_GetObjectItem(blob, "progress")->valueint);
         else
         {
            cJSON * blobsize_done_obj=cJSON_GetObjectItem(blob, "size_done");
            if (blobsize_done_obj)
            {
            char formatbuf[FORMAT_SIZE_BUF];
            Write_dyn_trace(trace,none,"   %s",format_size(formatbuf, blobsize_done_obj->valueint));
            }
         }
      }
      else Write_dyn_trace(trace,yellow,"         ");
     // Write_dyn_trace(trace, none, "%s",(char *)tt[trace->count]);
      Write_dyn_trace(trace, none, "\n");
   }
   if (trace->time_mark) update_time_dyn_trace(trace);
   if (final) set_mark_dynamic_trace(trace);
   if (!final)
   {
      dyn_trace_add_decrement(trace);
      update_details(trace);
   }
   clear_till_mark_dynamic_trace(trace);
   
   return;
}


void init_blobs_transfer(struct dl_blobs_transfer_s * dl_blobs_transfer,  char * from_image, char * to_image, cJSON * manifest, struct dkr_api_data_s * api_data )
{
   int i=0;
   cJSON * blobList=NULL;
   if (manifest)
   {
      cJSON * version_obj=cJSON_GetObjectItem(manifest, "schemaVersion");
      if (version_obj)
      {
         if (version_obj && cJSON_IsNumber(version_obj))
         {
            blobList=create_bloblist(manifest, version_obj->valueint);
         }
      }
   }
   int count=cJSON_GetArraySize(blobList);
   
   dl_blobs_transfer->from_image=from_image;
   dl_blobs_transfer->to_image=to_image;
   dl_blobs_transfer->blobs_list=blobList;
   dl_blobs_transfer->free_count=NUMT;
   dl_blobs_transfer->org_api_data=api_data;
   for(i=0; i< NUMT; i++) {
      dl_blobs_transfer->data[i].thread_Number=i;
      dl_blobs_transfer->data[i].finish=0;
      dl_blobs_transfer->data[i].free=1;
      dl_blobs_transfer->data[i].blob=NULL;
      dl_blobs_transfer->data[i].progress=0;
      dl_blobs_transfer->data[i].from_image=&dl_blobs_transfer->from_image;
      dl_blobs_transfer->data[i].to_image=&dl_blobs_transfer->to_image;
      dl_blobs_transfer->data[i].transfer_buffer=NULL;
      dl_blobs_transfer->data[i].finish=0;
      dl_blobs_transfer->data[i].Buff_empty=1;
      dl_blobs_transfer->data[i].Size_to_transfer=-1;
      pthread_mutex_init(&dl_blobs_transfer->data[i].buffer_lock, NULL);
 
      pthread_cond_init(&dl_blobs_transfer->data[i].s_buff_is_not_empty, NULL);
      pthread_cond_init(&dl_blobs_transfer->data[i].s_buff_is_empty, NULL);
      dl_blobs_transfer->data[i].url_down=NULL;
      dl_blobs_transfer->data[i].url_up=NULL;
      dl_blobs_transfer->data[i].Auth_down=NULL;
      dl_blobs_transfer->data[i].Auth_up=NULL;
      dl_blobs_transfer->data[i].curl_down=curl_easy_init();
      dl_blobs_transfer->data[i].curl_up=curl_easy_init();
	   curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_USERAGENT, PACKAGE_STRING);
	   curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_USERAGENT, PACKAGE_STRING);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_CAINFO, api_data->default_ca_bundle);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_SSL_VERIFYPEER, 1L);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_CAINFO, api_data->default_ca_bundle);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_SSL_VERIFYPEER, 1L);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_FOLLOWLOCATION, 0L); // need to clear Auth on redirect
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_WRITEFUNCTION, &cb_data_down);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_WRITEDATA,&dl_blobs_transfer->data[i]);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_READFUNCTION, &cb_data_up);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_READDATA,&dl_blobs_transfer->data[i]);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_BUFFERSIZE, 16372L);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_PROGRESSFUNCTION, &progress_transfer_up_old);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_PROGRESSDATA, &dl_blobs_transfer->data[i]);
#if LIBCURL_VERSION_NUM >= 0x072000
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_XFERINFOFUNCTION, &progress_transfer_up);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_XFERINFODATA, &dl_blobs_transfer->data[i]);
#endif
      
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_PROGRESSFUNCTION, &progress_transfer_down_old);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_PROGRESSDATA, &dl_blobs_transfer->data[i]);
#if LIBCURL_VERSION_NUM >= 0x072000
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_XFERINFOFUNCTION, &progress_transfer_down);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_XFERINFODATA, &dl_blobs_transfer->data[i]);
#endif
      /* enable progress meter */
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_NOPROGRESS, 0L);
       curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_NOPROGRESS, 0L);
      // todo enable progress download so it can be interupted
      
      if (*api_data->default_ca_bundle)
         curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_CAINFO, api_data->default_ca_bundle);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_SSL_VERIFYPEER, 1L);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_VERBOSE, 0L);
    
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_TIMEOUT, 0L); // no timeoutx
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_down, CURLOPT_CONNECTTIMEOUT,60L); // 30 sec on connection
      if (*api_data->default_ca_bundle)
         curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_CAINFO, api_data->default_ca_bundle);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_SSL_VERIFYPEER, 1L);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_VERBOSE, 0L);
      
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_TIMEOUT, 0L); // no timeoutx
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_CONNECTTIMEOUT,60L); // 30 sec on connection
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_PUT, 1L);
      curl_easy_setopt(dl_blobs_transfer->data[i].curl_up, CURLOPT_POST, 0L);
   }
   dl_blobs_transfer->todo=count;
   dl_blobs_transfer->next=-1;
   return;
}

void close_transfer_blobs(struct dl_blobs_transfer_s * dl_blobs)
{
   debug("waiting for unfinished threads to end\n");
   int i=0;
   for(i=0; i< NUMT; i++) {
      if (!dl_blobs->data[i].free)
      {
         pthread_join(dl_blobs->data[i].tid[0], NULL);
         pthread_join(dl_blobs->data[i].tid[1], NULL);
      }
      if (dl_blobs->data[i].transfer_buffer) free(dl_blobs->data[i].transfer_buffer);
      dl_blobs->data[i].transfer_buffer=NULL;
      dl_blobs->data[i].finish=0;
      dl_blobs->data[i].free=1;
      dl_blobs->data[i].blob=NULL;
	   if(dl_blobs->data[i].curl_down) curl_easy_cleanup(dl_blobs->data[i].curl_down);
	   if(dl_blobs->data[i].curl_up) curl_easy_cleanup(dl_blobs->data[i].curl_up);
	   dl_blobs->data[i].curl_down=NULL;
	   dl_blobs->data[i].curl_up=NULL;
      pthread_mutex_destroy(&dl_blobs->data[i].buffer_lock);
   }
   dl_blobs->free_count=NUMT;
	cJSON_Delete( dl_blobs->blobs_list);
	dl_blobs->blobs_list=NULL;
}

void cancel_transfer_blobs(struct dl_blobs_transfer_s * dl_blobs_transfer)
{
   debug("Canceling transfers\n");
   int i=0;
   for(i=0; i< NUMT; i++) {
      if (!dl_blobs_transfer->data[i].free)
      {
         pthread_cancel(dl_blobs_transfer->data[i].tid[0]);
         pthread_cancel(dl_blobs_transfer->data[i].tid[1]);
      }
   }
   return;
}



int progress_change_blobs(struct dl_blobs_transfer_s * dl_blobs_transfer)
{
   // Re-entrant check if there was any progress
   int i;
   int result=0;
   for(i=0; i< NUMT; i++)
   {
      if (dl_blobs_transfer->data[i].progress)
      {
         debug("prgress change\n");
         result= 1;
         dl_blobs_transfer->data[i].progress=0;
      }
   }
   return result;
}


int transfer_blobs_iteration(struct dl_blobs_transfer_s * dl_blobs)
{
   // Re-entrant check if something new to do
   // actions, if number of free < free max and next, create new transfer
   //          if thread finished, update free
   //          mark todo
   
   int i;
   int error = 0;
   // check if we have threads finished / maybe retry on error
   if (dl_blobs->free_count != NUMT)
   { //swoop and check if finished
      for(i=0; i< NUMT; i++) {
         if (dl_blobs->data[i].finish && !dl_blobs->data[i].free )
         {
            pthread_join(dl_blobs->data[i].tid[0], NULL); // shall not return EINTR
            pthread_join(dl_blobs->data[i].tid[0], NULL); // shall not return EINTR
            debug("Joined threads %s\n",cJSON_get_key(dl_blobs->data[i].blob, "sha256"));
            dl_blobs->data[i].finish=0;
            dl_blobs->data[i].free=1;
            dl_blobs->free_count++;
            if (dl_blobs->data[i].error !=0)
            {
               debug("got a download error, error_cout=%d\n",dl_blobs->data[i].error_count);
               dl_blobs->data[i].error_count++;
               cJSON_GetObjectItem(dl_blobs->data[i].blob, "error_count")->valueint=dl_blobs->data[i].error_count;
               cJSON_GetObjectItem(dl_blobs->data[i].blob, "status")->valueint=DR_TR_STAT_ERROR;
               dl_blobs->data[i].progress=1;
               if (dl_blobs->data[i].error_count <= 3)
               {
                  // and startit again
                  dl_blobs->todo++;
                  return dl_blobs->todo;
               }
               else
               {
                  return -1;
               }
            }
         }
      }
   }
   
   // do we still have something to do? if not and all threads free we're finished return 0;
   if (dl_blobs->todo==0 && dl_blobs->free_count == NUMT ) return 0;
   
   // do we do have something to do
   if (dl_blobs->todo > 0 )
   {
      debug("todo==%d, next download=%d, \n",dl_blobs->todo, dl_blobs->next);
      //if no free threads, sleep and comeback later
      if (dl_blobs->free_count == 0)
      {
            usleep(300000); // sleep a bit come back later, we got nothing else todo
         return (dl_blobs->todo);
      }
      // we have something to do, find free thread
      for(i=0; i< NUMT; i++) {
         if (dl_blobs->data[i].free==1) break;
      }
      
      error=start_a_blob_transfer(dl_blobs,i);
      if(0 != error)
         return error;
      dl_blobs->todo--;
      //dl_blobs->next++;
   }
   else
      usleep(300000);
   if (dl_blobs->todo==0  && dl_blobs->free_count!=NUMT) return NUMT-dl_blobs->free_count;
   return (dl_blobs->todo);
}

cJSON * find_next_blob(struct dl_blobs_transfer_s * dl_blobs)
{
   cJSON * blob=NULL;
   dkr_tr_code status=DR_TR_STAT_OK;
    int count=cJSON_GetArraySize(dl_blobs->blobs_list);
   do
   {
      dl_blobs->next++;
      if (dl_blobs->next>=cJSON_GetArraySize(dl_blobs->blobs_list)) dl_blobs->next=0;
      blob=cJSON_GetArrayItem(dl_blobs->blobs_list, dl_blobs->next);
      status=cJSON_GetObjectItem(blob, "status")->valueint;
      count--;
   } while( (status==DR_TR_STAT_OK || status==DR_TR_STAT_NA || status==DR_TR_STAT_TRANSFER || status==DR_TR_STAT_RETRY) && count>-1);
   debug("found next at %d\n",dl_blobs->next);
   if (count==-1)
   return NULL;
      else
   return(blob);
}

// ***************************
// Level 2 private Functions
// ***************************
static int start_a_blob_transfer(struct dl_blobs_transfer_s * dl_blobs,int thread_number)
{
   
   struct dkr_api_data_s * dkr_api_data=dl_blobs->org_api_data;
   
   dl_blobs->data[thread_number].blob=find_next_blob(dl_blobs);
   
   if (!dl_blobs->data[thread_number].blob)
   {
      dl_blobs->todo=0;
   	return 0;
   }
   const char * blobsum=cJSON_get_key(dl_blobs->data[thread_number].blob, "sha256");
	const char * image=cJSON_get_key(dl_blobs->data[thread_number].blob, "image");
   debug("\n============================================================\n START A BLOB transfer: %s\n============================================================\n",blobsum);
   cJSON * sizeobject=cJSON_GetObjectItem(dl_blobs->data[thread_number].blob, "size");
   if (sizeobject)
   dl_blobs->data[thread_number].Size_to_transfer =sizeobject->valueint;
   else
      dl_blobs->data[thread_number].Size_to_transfer=-1;
   int res=dkr_api_blob_check(dkr_api_data, dl_blobs->to_image, blobsum);
   if (res==200)
   {
      cJSON_GetObjectItem (dl_blobs->data[thread_number].blob, "status")->valueint= DR_TR_STAT_NA;
      cJSON_GetObjectItem(dl_blobs->data[thread_number].blob, "progress")->valueint=100;
      dl_blobs->data[thread_number].error_count=0;
      dl_blobs->data[thread_number].progress=1;
      // no need to proceed, target is ok
      return 0;
   }
   int error_1=0;
   int error_2=0;
   
   cJSON*status_json=cJSON_GetObjectItem (dl_blobs->data[thread_number].blob, "status") ;
   cJSON*error_count_json=cJSON_GetObjectItem(dl_blobs->data[thread_number].blob, "error_count");
   cJSON*progress_json=cJSON_GetObjectItem(dl_blobs->data[thread_number].blob, "progress");
   
   progress_json->valueint=0;
   status_json->valueint=DR_TR_STAT_TRANSFER;
   dl_blobs->data[thread_number].error=0;
   dl_blobs->data[thread_number].Buff_empty=1;
   dl_blobs->data[thread_number].progress=1;
   dl_blobs->data[thread_number].error_count=error_count_json->valueint;
   if (dl_blobs->data[thread_number].error_count>0)status_json->valueint=DR_TR_STAT_RETRY;
   char * pull_url=NULL;
   char * Auth=NULL;
   if (image)
   		res=dkr_api_blob_pull_get_url(dl_blobs->org_api_data, image, blobsum,&pull_url,&Auth);
	else
		res=dkr_api_blob_pull_get_url(dl_blobs->org_api_data, dl_blobs->from_image, blobsum,&pull_url,&Auth);
	
   if (res==1 && pull_url)
   {
      // now set curl request ... etc
      
      dl_blobs->data[thread_number].url_down=pull_url;
      if (Auth)
      {
         dl_blobs->data[thread_number].Auth_down=Auth;
         
      }
      pull_url=NULL;
      Auth=NULL;
   }
   else
   {
      if (pull_url) free(pull_url);
      if(Auth) free(Auth);
      Auth=NULL;
      pull_url=NULL;
      status_json->valueint=DR_TR_STAT_ERROR;
      error_count_json->valueint++;
      return -1;
   }
   debug("\n------------------------------------------------------------------\n Get Push URL: %s\n------------------------------------------------------------------\n",blobsum);

   
   char * push_url=NULL;
   dl_blobs->data[thread_number].Auth_up=NULL;
   res=RETRY_HTTP(dkr_api_blob_push_get_url(dl_blobs->org_api_data, dl_blobs->to_image, blobsum,&push_url,&Auth,NULL));
   if (res==202 && push_url)
   {
      // now set curl request ... etc
      dl_blobs->data[thread_number].url_up=push_url;
      //dl_blobs->data[thread_number].curl_down
      push_url=NULL;
      if (Auth)
      {
         dl_blobs->data[thread_number].Auth_up=Auth;
         Auth=NULL;
      }
   }
   else
   {
      if (push_url) free(push_url);
      status_json->valueint=DR_TR_STAT_ERROR;
      error_count_json->valueint++;
      if (dl_blobs->data[thread_number].Auth_down) free(dl_blobs->data[thread_number].Auth_down);
      if (dl_blobs->data[thread_number].url_down) free(dl_blobs->data[thread_number].url_down);
      dl_blobs->data[thread_number].Auth_down=NULL;
      dl_blobs->data[thread_number].url_down=NULL;
      return -1;
   }
   dl_blobs->data[thread_number].Buff_size=-1;
   dl_blobs->data[thread_number].Buff_empty=1; // start 0
   dl_blobs->data[thread_number].finish=0;
   if (dl_blobs->data[thread_number].transfer_buffer) free(dl_blobs->data[thread_number].transfer_buffer);
   dl_blobs->data[thread_number].transfer_buffer=NULL;
   //curl_easy_setopt(dl_blobs->data[thread_number].curl_down,CURLOPT_VERBOSE,1L);
   if ( res==202)
   {
      debug("\n------------------------------------------------------------------\n start thread Pull Blob: %s\n------------------------------------------------------------------\n",blobsum);

      error_1=pthread_create(&dl_blobs->data[thread_number].tid[0],
                             NULL, // default attributes please
                             pull_blob,
                             (void *)&dl_blobs->data[thread_number]);
      
      if(0 != error_1)
      {
         error( "Couldn't run thread number %d, errno %d\n", thread_number, error_1);
         dl_blobs->data[thread_number].error=1;
         if (dl_blobs->data[thread_number].Auth_down) free(dl_blobs->data[thread_number].Auth_down);
         if (dl_blobs->data[thread_number].url_down) free(dl_blobs->data[thread_number].url_down);
         if (dl_blobs->data[thread_number].url_up) free(dl_blobs->data[thread_number].url_up);
         if (dl_blobs->data[thread_number].Auth_up) free(dl_blobs->data[thread_number].Auth_up);
         dl_blobs->data[thread_number].Auth_down=NULL;
         dl_blobs->data[thread_number].Auth_up=NULL;
         dl_blobs->data[thread_number].url_down=NULL;
         dl_blobs->data[thread_number].url_up=NULL;
         return -1;
      }
      usleep(1000);
      debug("\n------------------------------------------------------------------\n start thread PUSH Blob: %s\n------------------------------------------------------------------\n",blobsum);
      //curl_easy_setopt(dl_blobs->data[thread_number].curl_up,CURLOPT_VERBOSE,1L);
      error_2=pthread_create(&dl_blobs->data[thread_number].tid[1],
                             NULL, // default attributes please
                             push_blob,
                             (void *)&dl_blobs->data[thread_number]);
      
      if(0 != error_2)
      {
         error( "Couldn't run thread number %d, errno %d\n", thread_number, error_1);
         // need to cancel above thread forcefully
         dl_blobs->data[thread_number].error=1;
         if (dl_blobs->data[thread_number].Auth_up) free(dl_blobs->data[thread_number].Auth_up);
         if (dl_blobs->data[thread_number].Auth_down) free(dl_blobs->data[thread_number].Auth_down);
         if (dl_blobs->data[thread_number].url_down) free(dl_blobs->data[thread_number].url_down);
         if (dl_blobs->data[thread_number].url_up) free(dl_blobs->data[thread_number].url_up);
         dl_blobs->data[thread_number].Auth_down=NULL;
         dl_blobs->data[thread_number].Auth_up=NULL;
         dl_blobs->data[thread_number].url_down=NULL;
         dl_blobs->data[thread_number].url_up=NULL;
         return -1;
      }
      dl_blobs->free_count--;
      dl_blobs->data[thread_number].free=0;
      dl_blobs->data[thread_number].error=0;
      dl_blobs->data[thread_number].progress=1;
   }
   return 0;
}

static cJSON * create_bloblist(cJSON *manifest,int version)
{
	debug("Create Bloblist from :\n");
	print_json(manifest);
   cJSON * result=NULL;
   cJSON * layers=NULL;
   if (version==1)
      layers=cJSON_GetObjectItem(manifest, "fsLayers");
   else
      layers=cJSON_GetObjectItem(manifest, "layers");
	
   cJSON * image_froms=cJSON_GetObjectItem(manifest, "from_image");
	cJSON * display_names=cJSON_GetObjectItem(manifest, "display_names");
   int	item_cnt=-1;
	if (image_froms) item_cnt=0;
	
   if (layers)
   {
      result=cJSON_CreateArray();
      cJSON * layer=NULL;
	   int count_empty=0;
      cJSON_ArrayForEach(layer, layers)
      {
		  cJSON * item=NULL;
		  
         if (version==1)
         {
			 // don't transfer empty layers
			 if(validate_key(layer, "blobSum","sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4")!=0 || count_empty==0)
			 {
				 if (!count_empty && validate_key(layer, "blobSum","sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4")==0) count_empty++;
				item=cJSON_CreateObject();
				cJSON_AddItemToArray(result, item);
				cJSON_AddStringToObject(item, "sha256", cJSON_get_key(layer, "blobSum"));
				 if (item_cnt!=-1)
				 {
					 if (image_froms)
						 cJSON_AddStringToObject(item, "image", cJSON_GetArrayItem(image_froms, item_cnt)->valuestring);
					 if (display_names)
						 cJSON_AddStringToObject(item, "name", cJSON_GetArrayItem(display_names, item_cnt)->valuestring);
					 item_cnt++;
				 }
				cJSON_AddNumberToObject(item, "size_done", 0);
			 }
			 else
			 {
				 count_empty++;
				 if (item_cnt!=-1) item_cnt++;
				 debug("skip empty layer sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4\n");
			 }
         }
         else
         {
			item=cJSON_CreateObject();
			cJSON_AddItemToArray(result, item);
            cJSON_AddStringToObject(item, "sha256", cJSON_get_key(layer, "digest"));
			 if (item_cnt!=-1)
			 {
				 if (image_froms)
				 cJSON_AddStringToObject(item, "image", cJSON_GetArrayItem(image_froms, item_cnt)->valuestring);
				 if (display_names)
				 cJSON_AddStringToObject(item, "name", cJSON_GetArrayItem(display_names, item_cnt)->valuestring);
				 item_cnt++;
			 }
            // todo add size for progress
            cJSON * size_obj=cJSON_GetObjectItem(layer, "size");
            if (size_obj && cJSON_IsNumber(size_obj)) cJSON_AddNumberToObject(item, "size", size_obj->valueint);
         }
		  if (item)
		  {
			 cJSON_AddNumberToObject(item, "status", DR_TR_STAT_PENDING);
			 cJSON_AddNumberToObject(item, "progress", 0);
			 cJSON_AddNumberToObject(item, "error_count", 0);
		  }
      }
   }
	print_json(result);
   return result;
}


// *************************************************
// level 3 private functions, curl specific workers
// *************************************************

int progress_transfer_up(void *userp, __attribute__((unused)) curl_off_t dltotal, __attribute__((unused))curl_off_t dlnow,
                          __attribute__((unused))curl_off_t ultotal, curl_off_t ulnow)
{
   struct dl_blob_thread_s *data = (struct dl_blob_thread_s *)userp;
   if (data->error) return 1;
   CURL *curl = data->curl_up;
   double curtime = 0;
   curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &curtime);
   double lastruntime= data->lastruntime;
   if((curtime - lastruntime) >= 10)
   {
      data->lastruntime = curtime;
      debug( "DL TIME: %f => %d / %d \r\n", curtime,(int)ulnow,data->Size_to_transfer);
      int progress=-1;
      if (data->Size_to_transfer !=-1 && data->Size_to_transfer !=0)
      {
         progress=(100LL*(int)ulnow)/data->Size_to_transfer;
          cJSON_GetObjectItem(data->blob, "progress")->valueint=progress;
      }
      else
      {
         cJSON * size_done_obj=cJSON_GetObjectItem(data->blob, "size_done");
         if (size_done_obj) size_done_obj->valueint=(int)ulnow;
      }
     
      data->progress=1;
   }
   return 0;
}


int progress_transfer_up_old(void *userp,   double dltotal,   double dlnow,   double ultotal,   double ulnow)
{
   return progress_transfer_up(userp, (curl_off_t)dltotal,
                               (curl_off_t)dlnow,
                               (curl_off_t)ultotal,
                               (curl_off_t)ulnow);
}

int progress_transfer_down(void *userp, __attribute__((unused)) curl_off_t dltotal, __attribute__((unused)) curl_off_t dlnow,
                           __attribute__((unused)) curl_off_t ultotal, __attribute__((unused)) curl_off_t ulnow)
{
   struct dl_blob_thread_s *data = (struct dl_blob_thread_s *)userp;
   if (data->error) return 1;
   //( return non null for call back interuption)
   return 0;
}


int progress_transfer_down_old(void *userp,   double dltotal,   double dlnow,   double ultotal,   double ulnow)
{
   return progress_transfer_down(userp, (curl_off_t)dltotal,
                                 (curl_off_t)dlnow,
                                 (curl_off_t)ultotal,
                                 (curl_off_t)ulnow);
}

void cancel_upload(void * userp)
{
	struct dl_blob_thread_s *data = (struct dl_blob_thread_s *)userp;
	
 	debug("Signal Cancel upload \n");

	pthread_mutex_lock(&data->buffer_lock);
	if (!data->error)
		while (!data->Buff_empty)  // if we got error, don't wait
		{
			pthread_cond_wait(&data->s_buff_is_empty,&data->buffer_lock);
		}
	if (data->transfer_buffer)free(data->transfer_buffer);
	data->transfer_buffer=NULL;
	data->Buff_empty=0;
	data->Buff_size=0;
	pthread_mutex_unlock(&data->buffer_lock);
	pthread_cond_signal(&data->s_buff_is_not_empty);
}

size_t cb_data_down(void *contents, size_t size, size_t nmemb, void *userp)
{
   size_t realsize = size * nmemb;
   struct dl_blob_thread_s *data = (struct dl_blob_thread_s *)userp;
   void * tmp=NULL;
	
	
	
   pthread_mutex_lock(&data->buffer_lock);
   while (!data->Buff_empty)
   {
      pthread_cond_wait(&data->s_buff_is_empty,&data->buffer_lock);
   }
   long http_code=0;
   curl_easy_getinfo(data->curl_down, CURLINFO_HTTP_CODE, &http_code);

   if (http_code!=200)
   {
      debug("callback down httpcode=%d",http_code);
      pthread_mutex_unlock(&data->buffer_lock);
      return realsize;
   }
   if (!data->error)
   {
	   
	   if (data->size_received==0 && data->Size_to_transfer ==-1)
	   {
		   cJSON * content_LEN=cJSON_GetObjectItem(data->headers_pull.header, "Content-Length");
		   if (content_LEN && cJSON_IsNumber(content_LEN) && content_LEN->valueint>0)
		   {
			   data->Size_to_transfer=content_LEN->valueint;
			   cJSON_AddNumberToObject(data->blob, "size", data->Size_to_transfer);
		   }
	   }
	   if (realsize)
		   tmp= calloc(size,nmemb);
      if (tmp && contents && realsize) memcpy(tmp, contents, realsize);
	  data->size_received=data->size_received+realsize;
      data->transfer_buffer=tmp;
      data->Buff_empty=0;
      data->Buff_size=realsize;
      pthread_mutex_unlock(&data->buffer_lock);
      pthread_cond_signal(&data->s_buff_is_not_empty);
      return realsize;
   }
   else
   {
	  debug("cb_down data error\n");
	   data->transfer_buffer=NULL;
	   data->Buff_empty=0;
	   data->Buff_size=0;
	   pthread_mutex_unlock(&data->buffer_lock);
	   pthread_cond_signal(&data->s_buff_is_not_empty);
      return 0;
   }
}


size_t cb_data_up(void *contents, size_t size, size_t nmemb, void *userp)
{
   size_t realsize = size * nmemb;
   struct dl_blob_thread_s *data = (struct dl_blob_thread_s *)userp;
	
   pthread_mutex_lock(&data->buffer_lock);
   while (data->Buff_empty )
   {
      //debug("%%% data_up Waiting for Buffer not empty\n");
      int ret=pthread_cond_wait(&data->s_buff_is_not_empty,&data->buffer_lock);
      if(ret!=0) error("Thread cond wait %d: %s",data->thread_Number,strerror(errno));
   }
   if (!data->error )//(&& (realsize && (realsize >= data->Buff_size)) )
   {
	   
      if (data->Buff_size && data->transfer_buffer)
         memcpy(contents, data->transfer_buffer,  data->Buff_size);
	  if (data->Buff_size==0) debug("cb_up: Returning 0, end transfer\n");
      size_t buflen=data->Buff_size;
      if (data->transfer_buffer)free(data->transfer_buffer);
      data->transfer_buffer=NULL;
      data->Buff_empty=1;
      data->Buff_size=0;
	   
      pthread_mutex_unlock(&data->buffer_lock);
      pthread_cond_signal(&data->s_buff_is_empty);
      return buflen;
   }
   else
   {
	   if (data->transfer_buffer)free(data->transfer_buffer);
	   data->transfer_buffer=NULL;
      if (data->Buff_size && realsize && !(realsize >= data->Buff_size))
         debug("cb_up data size bigger then upload buffer\n");
      else if(data->Buff_empty)
         debug("cb_up databuff empty\n");
      else if (data->error)
         debug("cb_up had data error when loose\n");
      else
         debug("cb_up some eror\n");
	   data->Buff_empty=1;
	   data->Buff_size=0;
      pthread_mutex_unlock(&data->buffer_lock);
	   pthread_cond_signal(&data->s_buff_is_empty);
      return CURL_READFUNC_ABORT;
   }
}

void * push_blob(void * arg)
{
   long http_code=0;
   struct dl_blob_thread_s *data = (struct dl_blob_thread_s *)arg;
	
   int expect_100=1;
   data->lastruntime=0;
   //char tmp[4096];
   //snprintf(tmp, 4096,"Authorization: %s",data->Auth_up);
   //headers = curl_slist_append(headers,tmp);
   //headers = curl_slist_append(headers, "Content-Type: application/octet-stream");
   char * auth=data->Auth_up;
   char * request=data->url_up;
   int stop;
   data->error=0;
   do
   {
      
        struct curl_slist *headers = NULL;
      stop=1;
	   
      curl_headers_init(&data->headers_push);
      dynbuffer * chunk=dynbuffer_init();
      
      if(auth)
      {
         char tmp[4096];
         snprintf(tmp, 4096,"Authorization: %s",auth);
         headers = curl_slist_append(headers,tmp);
      }
      curl_easy_setopt(data->curl_up, CURLOPT_URL,request );
      curl_easy_setopt(data->curl_up, CURLOPT_HEADERFUNCTION,curl_headers_cb);
      curl_easy_setopt(data->curl_up, CURLOPT_WRITEHEADER, &data->headers_push);
      /* HTTP PUT please */
      curl_easy_setopt(data->curl_up, CURLOPT_PUT, 1L);
      /* enable uploading */
      curl_easy_setopt(data->curl_up, CURLOPT_UPLOAD, 1L);
/* if (size)
   {
       curl_easy_setopt(data->curl_up, CURLOPT_POSTFIELDSIZE, (long)size->valueint);
       debug("curl data size set to %d\n",(long)size->valueint);
   curl_easy_setopt(data->curl_up, CURLOPT_INFILESIZE_LARGE,
   (curl_off_t)0);
   }
   else*/
   {
      debug("No Size set\n");
    	headers = curl_slist_append(headers, "Transfer-Encoding: chunked");
   //   curl_easy_setopt(data->curl_up, CURLOPT_INFILESIZE_LARGE,(curl_off_t)0);
   }
	   if (expect_100)
      headers = curl_slist_append(headers, "Expect: 100-Continue");
      else
      headers = curl_slist_append(headers, "Expect: ");
      
      curl_easy_setopt(data->curl_up, CURLOPT_HTTPHEADER, headers);
         
         /* send all data to this function  */
         curl_easy_setopt(data->curl_up, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
         /* we pass our 'chunk' struct to the callback function */
         curl_easy_setopt(data->curl_up, CURLOPT_WRITEDATA, chunk);
         
      long res=curl_easy_perform(data->curl_up);
      curl_easy_getinfo (data->curl_up, CURLINFO_RESPONSE_CODE, &http_code);
      curl_slist_free_all(headers);
      if (http_code == 417)
	  {
		  stop=0;
		  expect_100=0;
		  debug("upload %s, got 417\n",cJSON_get_key(data->blob, "sha256"));
	  }
      else if (http_code == 201 && res != CURLE_ABORTED_BY_CALLBACK)
      {
         cJSON_GetObjectItem(data->blob, "status")->valueint=DR_TR_STAT_OK;
         debug("Finish upload %s\n",cJSON_get_key(data->blob, "sha256"));
		  stop=1;
      }
      else if (!data->error)
      {
         data->error=1;
         if (res)
            debug( "PUSH curl failed for %.*s : %s\n",16,cJSON_get_key(data->blob, "sha256"),curl_easy_strerror((CURLcode)res));
         else {
            debug("PUSH HTTP_code: %ld for %s\n",http_code,cJSON_get_key(data->blob, "sha256"));
         }
         if (dynbuffer_len(chunk)>1 )
         {
            debug("PUSH for %.*s  data: %s\n",16,cJSON_get_key(data->blob, "sha256"),dynbuffer_get(chunk));
         }
         cJSON_GetObjectItem(data->blob, "status")->valueint=DR_TR_STAT_ERROR;
         data->Buff_empty=0;
         pthread_cond_signal(&data->s_buff_is_empty); // get pull out of wait
      }
      else
         debug("PUSH exit, already error\n");
      curl_headers_free(&data->headers_push);
      dynbuffer_clear(&chunk);
   } while(!stop);
   if (request!=data->url_down) free(request);
   data->finish=1;
   return NULL;
}


void * pull_blob(void * arg)
{
   long http_code=0;
   struct dl_blob_thread_s *data = (struct dl_blob_thread_s *)arg;
	char * request=NULL;
	if (data->url_down)
		request=strdup(data->url_down);
	if (!request)
	{
		error("no request for %s blob pull\n",cJSON_get_key(data->blob, "sha256"));
	}
   curl_easy_setopt(data->curl_down, CURLOPT_URL,data->url_down );
   char * auth=data->Auth_down;
	
   data->error=0;
	data->size_received=0;
   //cJSON * size=cJSON_GetObjectItem( data->blob,"size");
   int stop;
   do
   {
      http_code=0;
      stop=1;
           curl_headers_init(&data->headers_pull);

      curl_easy_setopt(data->curl_down, CURLOPT_URL,request);
      struct curl_slist *headers = NULL;
      if(auth)
      {
         char tmp[4096];
         snprintf(tmp, 4096,"Authorization: %s",auth);
         headers = curl_slist_append(headers,tmp);
      }
      curl_easy_setopt(data->curl_down, CURLOPT_HTTPHEADER, headers);
      
      curl_easy_setopt(data->curl_down, CURLOPT_HEADERFUNCTION,curl_headers_cb);
      curl_easy_setopt(data->curl_down, CURLOPT_WRITEHEADER, &data->headers_pull);
      
      long res=curl_easy_perform(data->curl_down);
      debug("Finish Get\n");
      curl_easy_getinfo (data->curl_down, CURLINFO_RESPONSE_CODE, &http_code);
      curl_slist_free_all(headers);
	   
      if (http_code ==307 )
      {
         const char * Location=cJSON_get_key(data->headers_pull.header, "Location");
		  
         if (Location)
		 {
			 // need solution with a bool indicating request is dynamic or not to avoid analyser to indicate memory leak
			 free(request);
			 request=strdup(Location);
		 }
		 auth=NULL;
         stop=0;
         debug("\npull got %d for %.*s: %s \n",http_code,16,cJSON_get_key(data->blob, "sha256"),Location);
         data->error=0;
      }
      else if (http_code == 200 && !data->error)
      {
		  
         if (res!=CURLE_OK)
            debug( "PULL curl failed for %s : %s\n",cJSON_get_key(data->blob, "sha256"),curl_easy_strerror((CURLcode)res));
         else
         {
         	debug("PULL Finish download %s\n",cJSON_get_key(data->blob, "sha256"));
         	
         }
		  cancel_upload(data);
		  debug("pull blob: back from cancel\n");
          stop=1;
      }
      else if (!data->error)
      {
         data->error=1;
         if (res)
            debug( "PULL curl failed for %s : %s\n",cJSON_get_key(data->blob, "sha256"),curl_easy_strerror((CURLcode)res));
         else {
            debug("PULL HTTP_code: %ld for %s\n",http_code,cJSON_get_key(data->blob, "sha256"));
         }
         cJSON_GetObjectItem(data->blob, "status")->valueint=DR_TR_STAT_ERROR;
		  cancel_upload(data);
         debug("pull blob: back from cancel\n");
		  
		  stop=1;
      }
	   else if(data->error)
	   {
		   debug("exit upload due to data_error\n");
	   }
      curl_headers_free(&data->headers_pull);
      
   } while(!stop);
   free(request);
   return NULL;
}

