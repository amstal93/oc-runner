//
//  jws.c
//  cjose_cjson
//
//  Created by Danny Goossen on 22/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//



/*!
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2014-2016 Cisco Systems, Inc.  All Rights Reserved.
 */


#include <string.h>
#include <assert.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/err.h>
#include <openssl/hmac.h>

#include "cJSON_deploy.h"
#include "b64.h"
#include "jws.h"
#include "jwk.h"

const char *JOSE_HDR_ALG = "alg";
const char *JOSE_HDR_ALG_NONE = "none";
const char *JOSE_HDR_ALG_RSA_OAEP = "RSA-OAEP";
const char *JOSE_HDR_ALG_RSA1_5 = "RSA1_5";
const char *JOSE_HDR_ALG_A128KW = "A128KW";
const char *JOSE_HDR_ALG_A192KW = "A192KW";
const char *JOSE_HDR_ALG_A256KW = "A256KW";
const char *JOSE_HDR_ALG_DIR = "dir";
const char *JOSE_HDR_ALG_PS256 = "PS256";
const char *JOSE_HDR_ALG_PS384 = "PS384";
const char *JOSE_HDR_ALG_PS512 = "PS512";
const char *JOSE_HDR_ALG_RS256 = "RS256";
const char *JOSE_HDR_ALG_RS384 = "RS384";
const char *JOSE_HDR_ALG_RS512 = "RS512";
const char *JOSE_HDR_ALG_HS256 = "HS256";
const char *JOSE_HDR_ALG_HS384 = "HS384";
const char *JOSE_HDR_ALG_HS512 = "HS512";
const char *JOSE_HDR_ALG_ES256 = "ES256";
const char *JOSE_HDR_ALG_ES384 = "ES384";
const char *JOSE_HDR_ALG_ES512 = "ES512";

const char *JOSE_HDR_ENC = "enc";
const char *JOSE_HDR_ENC_A256GCM = "A256GCM";
const char *JOSE_HDR_ENC_A128CBC_HS256 = "A128CBC-HS256";
const char *JOSE_HDR_ENC_A192CBC_HS384 = "A192CBC-HS384";
const char *JOSE_HDR_ENC_A256CBC_HS512 = "A256CBC-HS512";

const char *JOSE_HDR_CTY = "cty";

const char *JOSE_HDR_KID = "kid";

// Internals for referencing
static bool jws_build_dig_sha(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

static bool jws_build_sig_ps(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

static bool jws_build_dig_hmac_sha(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

static bool jws_verify_sig_ps(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

static bool jws_build_sig_rs(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

static bool jws_verify_sig_rs(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

static bool jws_build_sig_hmac_sha(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

static bool jws_verify_sig_hmac_sha(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

static bool jws_build_sig_ec(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

static bool jws_verify_sig_ec(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err);

// Internal help functions

static int cjose_const_memcmp(const uint8_t *a, const uint8_t *b, const size_t size)
{
   unsigned char result = 0;
   for (size_t i = 0; i < size; i++)
   {
      result |= a[i] ^ b[i];
   }
   
   return result;
}

static bool jws_build_hdr(cjose_jws_t *jws, const cJSON *header, jose_err *err)
{
   // save header object as part of the JWS (and incr. refcount)
   jws->hdr = cJSON_Duplicate(header,1); //make a dup when we known what header protectec/unprotected
   
   // base64url encode the header
   char *hdr_str = cJSON_PrintUnformatted(jws->hdr);
   if (NULL == hdr_str)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      return false;
   }
   if (!base64url_encode((const uint8_t *)hdr_str, strlen(hdr_str), &jws->hdr_b64u, &jws->hdr_b64u_len, err))
   {
      free(hdr_str);
      return false;
   }
   free(hdr_str);
   
   return true;
}


static bool jws_set_alg_funct(cjose_jws_t *jws,const char *alg, jose_err *err)
{
    if (err) err=err;
   jws->alg=strdup(alg);
   
   if ((strcmp(alg, JOSE_HDR_ALG_PS256) == 0) || (strcmp(alg, JOSE_HDR_ALG_PS384) == 0)
       || (strcmp(alg, JOSE_HDR_ALG_PS512) == 0))
   {
      jws->fns.digest = jws_build_dig_sha;
      jws->fns.sign = jws_build_sig_ps;
      jws->fns.verify = jws_verify_sig_ps;
   }
   else if ((strcmp(alg, JOSE_HDR_ALG_RS256) == 0) || (strcmp(alg, JOSE_HDR_ALG_RS384) == 0)
            || (strcmp(alg, JOSE_HDR_ALG_RS512) == 0))
   {
      jws->fns.digest = jws_build_dig_sha;
      jws->fns.sign = jws_build_sig_rs;
      jws->fns.verify = jws_verify_sig_rs;
   }
   else if ((strcmp(alg, JOSE_HDR_ALG_HS256) == 0) || (strcmp(alg, JOSE_HDR_ALG_HS384) == 0)
            || (strcmp(alg, JOSE_HDR_ALG_HS512) == 0))
   {
      jws->fns.digest = jws_build_dig_hmac_sha;
      jws->fns.sign = jws_build_sig_hmac_sha;
      jws->fns.verify = jws_verify_sig_hmac_sha;
   }
   else if ((strcmp(alg, JOSE_HDR_ALG_ES256) == 0) || (strcmp(alg, JOSE_HDR_ALG_ES384) == 0)
            || (strcmp(alg, JOSE_HDR_ALG_ES512) == 0))
   {
      jws->fns.digest = jws_build_dig_sha;
      jws->fns.sign = jws_build_sig_ec;
      jws->fns.verify = jws_verify_sig_ec;
   
   }
   else return false;
   return true;
}

static bool jws_build_dat(cjose_jws_t *jws, const uint8_t *plaintext, size_t plaintext_len, jose_err *err)
{
   // copy plaintext data
   jws->dat_len = plaintext_len;
   jws->dat = calloc(1,jws->dat_len+1);
   if (NULL == jws->dat)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      return false;
   }
   memcpy(jws->dat, plaintext, jws->dat_len);
   
   // base64url encode data
   if (!base64url_encode((const uint8_t *)plaintext, plaintext_len, &jws->dat_b64u, &jws->dat_b64u_len, err))
   {
      return false;
   }
   
   return true;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_build_dig_sha(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   bool retval = false;
   EVP_MD_CTX *ctx = NULL;
   // avoid compiler complains of un used params
   if (jwk) retval = false;
  
   const char *alg = jws->alg;
   
   // build digest using SHA-256/384/512 digest algorithm
   const EVP_MD *digest_alg = NULL;
   if ((strcmp(alg, JOSE_HDR_ALG_RS256) == 0) || (strcmp(alg, JOSE_HDR_ALG_PS256) == 0)
       || (strcmp(alg, JOSE_HDR_ALG_ES256) == 0))
      digest_alg = EVP_sha256();
   else if ((strcmp(alg, JOSE_HDR_ALG_RS384) == 0) || (strcmp(alg, JOSE_HDR_ALG_PS384) == 0)
            || (strcmp(alg, JOSE_HDR_ALG_ES384) == 0))
      digest_alg = EVP_sha384();
   else if ((strcmp(alg, JOSE_HDR_ALG_RS512) == 0) || (strcmp(alg, JOSE_HDR_ALG_PS512) == 0)
            || (strcmp(alg, JOSE_HDR_ALG_ES512) == 0))
      digest_alg = EVP_sha512();
   
   if (NULL == digest_alg)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_sha_cleanup;
   }
   
   // allocate buffer for digest
   jws->dig_len = EVP_MD_size(digest_alg);
   jws->dig = calloc(1,jws->dig_len);
   if (NULL == jws->dig)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      goto jws_build_dig_sha_cleanup;
   }
   
   // instantiate and initialize a new mac digest context
   ctx = EVP_MD_CTX_create();
   if (NULL == ctx)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_sha_cleanup;
   }
   EVP_MD_CTX_init(ctx);
   
   // create digest as DIGEST(B64U(HEADER).B64U(DATA))
   if (EVP_DigestInit_ex(ctx, digest_alg, NULL) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_sha_cleanup;
   }
   if (EVP_DigestUpdate(ctx, jws->hdr_b64u, jws->hdr_b64u_len) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_sha_cleanup;
   }
   if (EVP_DigestUpdate(ctx, ".", 1) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_sha_cleanup;
   }
   if (EVP_DigestUpdate(ctx, jws->dat_b64u, jws->dat_b64u_len) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_sha_cleanup;
   }
   if (EVP_DigestFinal_ex(ctx, jws->dig, NULL) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_sha_cleanup;
   }
   
   // if we got this far - success
   retval = true;
   
jws_build_dig_sha_cleanup:
   if (NULL != ctx)
   {
      EVP_MD_CTX_destroy(ctx);
   }
   
   return retval;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_build_dig_hmac_sha(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   bool retval = false;
   HMAC_CTX *ctx = NULL;
   
   const char *alg = jws->alg;
   
   // build digest using SHA-256/384/512 digest algorithm
   const EVP_MD *digest_alg = NULL;
   if (strcmp(alg, JOSE_HDR_ALG_HS256) == 0)
      digest_alg = EVP_sha256();
   else if (strcmp(alg, JOSE_HDR_ALG_HS384) == 0)
      digest_alg = EVP_sha384();
   else if (strcmp(alg, JOSE_HDR_ALG_HS512) == 0)
      digest_alg = EVP_sha512();
   
   if (NULL == digest_alg)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_hmac_sha_cleanup;
   }
   
   // allocate buffer for digest
   jws->dig_len = EVP_MD_size(digest_alg);
   jws->dig = calloc(1,jws->dig_len);
   if (NULL == jws->dig)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      goto jws_build_dig_hmac_sha_cleanup;
   }
   
   // instantiate and initialize a new mac digest context
   #if (JOSE_OPENSSL_11X)
      ctx = HMAC_CTX_new();
#else
   ctx = calloc(1,sizeof(HMAC_CTX));
#endif
   if (NULL == ctx)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      goto jws_build_dig_hmac_sha_cleanup;
   }
   
#if !(JOSE_OPENSSL_11X)
   HMAC_CTX_init(ctx);
#endif
   
   // create digest as DIGEST(B64U(HEADER).B64U(DATA))
   if (HMAC_Init_ex(ctx, jwk->keydata, (int)(jwk->keysize / 8), digest_alg, NULL) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_hmac_sha_cleanup;
   }
   if (HMAC_Update(ctx, (const unsigned char *)jws->hdr_b64u, jws->hdr_b64u_len) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_hmac_sha_cleanup;
   }
   if (HMAC_Update(ctx, (const unsigned char *)".", 1) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_hmac_sha_cleanup;
   }
   if (HMAC_Update(ctx, (const unsigned char *)jws->dat_b64u, jws->dat_b64u_len) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_hmac_sha_cleanup;
   }
   if (HMAC_Final(ctx, jws->dig, NULL) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_dig_hmac_sha_cleanup;
   }
   
   // if we got this far - success
   retval = true;
   
jws_build_dig_hmac_sha_cleanup:
   if (NULL != ctx)
   {
#if (JOSE_OPENSSL_11X)
      HMAC_CTX_free(ctx);
#else
      HMAC_CTX_cleanup(ctx);
      free(ctx);
#endif
   }
   
   return retval;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_build_sig_ps(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   bool retval = false;
   uint8_t *em = NULL;
   size_t em_len = 0;
   
   // ensure jwk is private RSA
   if (jwk->kty != JOSE_JWK_KTY_RSA)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      goto jws_build_sig_ps_cleanup;
   }
   RSA *rsa = (RSA *)jwk->keydata;
   BIGNUM *rsa_n = NULL, *rsa_e = NULL, *rsa_d = NULL;
   jwk_rsa_get(rsa, &rsa_n, &rsa_e, &rsa_d);
   if (!rsa || !rsa_e || !rsa_n || !rsa_d)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   
   const char *alg = jws->alg;
   
   // build digest using SHA-256/384/512 digest algorithm
   const EVP_MD *digest_alg = NULL;
   if (strcmp(alg, JOSE_HDR_ALG_PS256) == 0)
      digest_alg = EVP_sha256();
   else if (strcmp(alg, JOSE_HDR_ALG_PS384) == 0)
      digest_alg = EVP_sha384();
   else if (strcmp(alg, JOSE_HDR_ALG_PS512) == 0)
      digest_alg = EVP_sha512();
   
   if (NULL == digest_alg)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_sig_ps_cleanup;
   }
   
   // apply EMSA-PSS encoding (RFC-3447, 8.1.1, step 1)
   // (RSA_padding_add_PKCS1_PSS includes PKCS1_MGF1, -1 => saltlen = hashlen)
   em_len = RSA_size((RSA *)jwk->keydata);
   em = calloc(1,em_len);
   if (NULL == em)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      goto jws_build_sig_ps_cleanup;
   }
   if (RSA_padding_add_PKCS1_PSS((RSA *)jwk->keydata, em, jws->dig, digest_alg, -1) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_sig_ps_cleanup;
   }
   
   // sign the digest (RFC-3447, 8.1.1, step 2)
   jws->sig_len = em_len;
   jws->sig = calloc(1,jws->sig_len);
   if (NULL == jws->sig)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      goto jws_build_sig_ps_cleanup;
   }
   
   if (RSA_private_encrypt((int)em_len, em, jws->sig, (RSA *)jwk->keydata, RSA_NO_PADDING) != jws->sig_len)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_sig_ps_cleanup;
   }
   
   // base64url encode signed digest
   if (!base64url_encode((const uint8_t *)jws->sig, jws->sig_len, &jws->sig_b64u, &jws->sig_b64u_len, err))
   {
      goto jws_build_sig_ps_cleanup;
   }
   
   // if we got this far - success
   retval = true;
   
jws_build_sig_ps_cleanup:
   free(em);
   
   return retval;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_build_sig_rs(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   // ensure jwk is private RSA
   if (jwk->kty != JOSE_JWK_KTY_RSA)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   RSA *rsa = (RSA *)jwk->keydata;
   BIGNUM *rsa_n = NULL, *rsa_e = NULL, *rsa_d = NULL;
   jwk_rsa_get(rsa, &rsa_n, &rsa_e, &rsa_d);
   if (!rsa || !rsa_e || !rsa_n || !rsa_d)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   
   // allocate buffer for signature
   jws->sig_len = RSA_size((RSA *)jwk->keydata);
   jws->sig = calloc(1,jws->sig_len);
   if (NULL == jws->sig)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      return false;
   }
   
   const char *alg = jws->alg;
   
   // build digest using SHA-256/384/512 digest algorithm
   int digest_alg = -1;
   if (strcmp(alg, JOSE_HDR_ALG_RS256) == 0)
      digest_alg = NID_sha256;
   else if (strcmp(alg, JOSE_HDR_ALG_RS384) == 0)
      digest_alg = NID_sha384;
   else if (strcmp(alg, JOSE_HDR_ALG_RS512) == 0)
      digest_alg = NID_sha512;
   if (-1 == digest_alg)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      return false;
   }
   
   unsigned int siglen;
   if (RSA_sign(digest_alg, jws->dig,(unsigned int) jws->dig_len, jws->sig, &siglen, (RSA *)jwk->keydata) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      return false;
   }
   jws->sig_len = siglen;
   
   // base64url encode signed digest
   if (!base64url_encode((const uint8_t *)jws->sig, jws->sig_len, &jws->sig_b64u, &jws->sig_b64u_len, err))
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      return false;
   }
   
   return true;
}

static bool jws_build_sig_hmac_sha(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   // ensure jwk is OCT
   if (jwk->kty != JOSE_JWK_KTY_OCT)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   
   // allocate buffer for signature
   jws->sig_len = jws->dig_len;
   jws->sig = calloc(1,jws->sig_len);
   if (NULL == jws->sig)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      return false;
   }
   
   memcpy(jws->sig, jws->dig, jws->sig_len);
   
   // base64url encode signed digest
   if (!base64url_encode((const uint8_t *)jws->sig, jws->sig_len, &jws->sig_b64u, &jws->sig_b64u_len, err))
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      return false;
   }
   
   return true;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_build_sig_ec(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   bool retval = false;
   
   // ensure jwk is EC
   if (jwk->kty != JOSE_JWK_KTY_EC)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   
   ec_keydata *keydata = (ec_keydata *)jwk->keydata;
   EC_KEY *ec = keydata->key;
   
   ECDSA_SIG *ecdsa_sig = ECDSA_do_sign(jws->dig,(unsigned int) jws->dig_len, ec);
   if (NULL == ecdsa_sig)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_sig_ec_cleanup;
   }
   
   // allocate buffer for signature
   switch (keydata->crv)
   {
      case JOSE_JWK_EC_P_256:
         jws->sig_len = 32 * 2;
         break;
      case JOSE_JWK_EC_P_384:
         jws->sig_len = 48 * 2;
         break;
      case JOSE_JWK_EC_P_521:
         jws->sig_len = 66 * 2;
         break;
   }
   
   jws->sig = calloc(1,jws->sig_len);
   if (NULL == jws->sig)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      goto jws_build_sig_ec_cleanup;
   }
   
   memset(jws->sig, 0, jws->sig_len);
   
   const BIGNUM *pr, *ps;
#if (JOSE_OPENSSL_11X)
   ECDSA_SIG_get0(ecdsa_sig, &pr, &ps);
#else
   pr = ecdsa_sig->r;
   ps = ecdsa_sig->s;
#endif
   
   int rlen = BN_num_bytes(pr);
   int slen = BN_num_bytes(ps);
   BN_bn2bin(pr, jws->sig + jws->sig_len / 2 - rlen);
   BN_bn2bin(ps, jws->sig + jws->sig_len - slen);
   
   // base64url encode signed digest
   if (!base64url_encode((const uint8_t *)jws->sig, jws->sig_len, &jws->sig_b64u, &jws->sig_b64u_len, err))
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_build_sig_ec_cleanup;
   }
   
   retval = true;
   
jws_build_sig_ec_cleanup:
   if (ecdsa_sig)
      ECDSA_SIG_free(ecdsa_sig);
   
   return retval;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_build_cser(cjose_jws_t *jws, jose_err *err)
{
   // both sign and import should be setting these - but check just in case
   if (NULL == jws->hdr_b64u || NULL == jws->dat_b64u || NULL == jws->sig_b64u)
   {
      return false;
   }
   
   // compute length of compact serialization
   jws->cser_len = jws->hdr_b64u_len + jws->dat_b64u_len + jws->sig_b64u_len + 3;
   
   // allocate buffer for compact serialization
   assert(NULL == jws->cser);
   jws->cser = (char *)calloc(1,jws->cser_len);
   if (NULL == jws->cser)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      return false;
   }
   
   // build the compact serialization
   snprintf(jws->cser, jws->cser_len, "%s.%s.%s", jws->hdr_b64u, jws->dat_b64u, jws->sig_b64u);
   
   return true;
}

////////////////////////////////////////////////////////////////////////////////
cjose_jws_t *cjose_jws_sign(
                            const cjose_jwk_t *jwk, cJSON *protected_header, const uint8_t *plaintext, size_t plaintext_len, jose_err *err)
{
   cjose_jws_t *jws = NULL;
   
   if (NULL == jwk || NULL == protected_header || NULL == plaintext)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return NULL;
   }
   
   // allocate and initialize JWS
   jws = (cjose_jws_t *)calloc(1,sizeof(cjose_jws_t));
   if (NULL == jws)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      return NULL;
   }
   memset(jws, 0, sizeof(cjose_jws_t));
   
   // build JWS header
   if (!jws_build_hdr(jws, protected_header, err))
   {
      cjose_jws_release(jws);
      return NULL;
   }
   
   // validate JWS header
   if (!jws_set_alg_funct(jws,"ES256" ,err))
   {
      cjose_jws_release(jws);
      return NULL;
   }
   
   // build the JWS data segment
   if (!jws_build_dat(jws, plaintext, plaintext_len, err))
   {
      cjose_jws_release(jws);
      return NULL;
   }
   
   // build JWS digest (hashed signing input value)
   if (!jws->fns.digest(jws, jwk, err))
   {
      cjose_jws_release(jws);
      return NULL;
   }
   
   // sign the JWS digest
   if (!jws->fns.sign(jws, jwk, err))
   {
      cjose_jws_release(jws);
      return NULL;
   }
   
   // build JWS compact serialization
   if (!jws_build_cser(jws, err))
   {
      cjose_jws_release(jws);
      return NULL;
   }
   
   return jws;
}

////////////////////////////////////////////////////////////////////////////////
void cjose_jws_release(cjose_jws_t *jws)
{
   if (NULL == jws)
   {
      return;
   }
   
   if (NULL != jws->hdr)
   {
      cJSON_Delete(jws->hdr);
   }
   if (jws->alg) free(jws->alg);
   if (jws->hdr_b64u ) free(jws->hdr_b64u);
   if (jws->dat )free(jws->dat);
   if (jws->dat_b64u )free(jws->dat_b64u);
   if (jws->dig )free(jws->dig);
   if (jws->sig )free(jws->sig);
   if (jws->sig_b64u )free(jws->sig_b64u);
   if (jws->cser )free(jws->cser);
   free(jws);
}

////////////////////////////////////////////////////////////////////////////////
bool cjose_jws_export(cjose_jws_t *jws, const char **compact, jose_err *err)
{
   if (NULL == jws || NULL == compact)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   
   if (NULL == jws->cser)
   {
      jws_build_cser(jws, err);
   }
   
   *compact = jws->cser;
   return true;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_strcpy(char **dst, const char *src, size_t len, jose_err *err)
{
   *dst = (char *)calloc(1,len + 1);
   if (NULL == dst)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   
   strncpy(*dst, src, len);
   (*dst)[len] = 0;
   
   return true;
}

////////////////////////////////////////////////////////////////////////////////
//todo need alg
cjose_jws_t *cjose_jws_import(const char *cser, size_t cser_len, jose_err *err)
{
   cjose_jws_t *jws = NULL;
   size_t len = 0;
   
   if (NULL == cser)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return NULL;
   }
   
   // allocate and initialize a new JWS object
   jws = (cjose_jws_t *)calloc(1,sizeof(cjose_jws_t));
   if (NULL == jws)
   {
      JOSE_ERROR(err, JOSE_ERR_NO_MEMORY);
      return NULL;
   }
   memset(jws, 0, sizeof(cjose_jws_t));
   
   // find the indexes of the dots
   int idx = 0;
   int d[2] = { 0, 0 };
   for (int i = 0; i < cser_len && idx < 2; ++i)
   {
      if (cser[i] == '.')
      {
         d[idx++] = i;
      }
   }
   
   // fail if we didn't find both dots
   if (0 == d[1])
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      cjose_jws_release(jws);
      return NULL;
   }
   
   // copy and decode header b64u segment
   uint8_t *hdr_str = NULL;
   jws->hdr_b64u_len = d[0];
   jws_strcpy(&jws->hdr_b64u, cser, (int)jws->hdr_b64u_len, err);
   if (!base64url_decode(jws->hdr_b64u, jws->hdr_b64u_len, &hdr_str, &len, err) || NULL == hdr_str)
   {
      cjose_jws_release(jws);
      return NULL;
   }
   
   // deserialize JSON header
   jws->hdr = cJSON_Parse((const char *)hdr_str);
   free(hdr_str);
   if (NULL == jws->hdr)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      cjose_jws_release(jws);
      return NULL;
   }
   
   // validate the JSON header segment // todo fix this
   if (!jws_set_alg_funct(jws,"ES-256", err))
   {
      // make an exception for alg=none so that it will import/parse but not sign/verify
      const char *alg = jws->alg;
      if ((!alg) || (strcmp(alg, JOSE_HDR_ALG_NONE) != 0))
      {
         JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
         cjose_jws_release(jws);
         return NULL;
      }
   }
   
   // copy and b64u decode data segment
   jws->dat_b64u_len = d[1] - d[0] - 1;
   jws_strcpy(&jws->dat_b64u, cser + d[0] + 1, jws->dat_b64u_len, err);
   if (!base64url_decode(jws->dat_b64u, jws->dat_b64u_len, &jws->dat, &jws->dat_len, err))
   {
      cjose_jws_release(jws);
      return NULL;
   }
   
   // copy and b64u decode signature segment
   jws->sig_b64u_len = cser_len - d[1] - 1;
   jws_strcpy(&jws->sig_b64u, cser + d[1] + 1, jws->sig_b64u_len, err);
   if (!base64url_decode(jws->sig_b64u, jws->sig_b64u_len, &jws->sig, &jws->sig_len, err))
   {
      cjose_jws_release(jws);
      return NULL;
   }
   
   return jws;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_verify_sig_ps(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   bool retval = false;
   uint8_t *em = NULL;
   size_t em_len = 0;
   
   // ensure jwk is RSA
   if (jwk->kty != JOSE_JWK_KTY_RSA)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      goto jws_verify_sig_ps_cleanup;
   }
   
   // make sure we have an alg header
   const char *alg = jws->alg;
   
   // build digest using SHA-256/384/512 digest algorithm
   const EVP_MD *digest_alg = NULL;
   if (strcmp(alg, JOSE_HDR_ALG_PS256) == 0)
      digest_alg = EVP_sha256();
   else if (strcmp(alg, JOSE_HDR_ALG_PS384) == 0)
      digest_alg = EVP_sha384();
   else if (strcmp(alg, JOSE_HDR_ALG_PS512) == 0)
      digest_alg = EVP_sha512();
   
   if (NULL == digest_alg)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_verify_sig_ps_cleanup;
   }
   
   // allocate buffer for encoded message
   em_len = RSA_size((RSA *)jwk->keydata);
   em = calloc(1,em_len);
   if (NULL == em)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_verify_sig_ps_cleanup;
   }
   
   // decrypt signature
   if (RSA_public_decrypt((int)jws->sig_len, jws->sig, em, (RSA *)jwk->keydata, RSA_NO_PADDING) != em_len)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_verify_sig_ps_cleanup;
   }
   
   // verify decrypted signature data against PSS encoded digest
   if (RSA_verify_PKCS1_PSS((RSA *)jwk->keydata, jws->dig, digest_alg, em, -1) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_verify_sig_ps_cleanup;
   }
   
   // if we got this far - success
   retval = true;
   
jws_verify_sig_ps_cleanup:
   free(em);
   
   return retval;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_verify_sig_rs(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   bool retval = false;
   
   // ensure jwk is RSA
   if (jwk->kty != JOSE_JWK_KTY_RSA)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      goto jws_verify_sig_rs_cleanup;
   }
   
   // make sure we have an alg header
   // make sure we have an alg header
   const char *alg = jws->alg;
   
   // build digest using SHA-256/384/512 digest algorithm
   int digest_alg = -1;
   if (strcmp(alg, JOSE_HDR_ALG_RS256) == 0)
      digest_alg = NID_sha256;
   else if (strcmp(alg, JOSE_HDR_ALG_RS384) == 0)
      digest_alg = NID_sha384;
   else if (strcmp(alg, JOSE_HDR_ALG_RS512) == 0)
      digest_alg = NID_sha512;
   if (-1 == digest_alg)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_verify_sig_rs_cleanup;
   }
   
   if (RSA_verify(digest_alg, jws->dig, (int)jws->dig_len, jws->sig,(int) jws->sig_len, (RSA *)jwk->keydata) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_verify_sig_rs_cleanup;
   }
   
   // if we got this far - success
   retval = true;
   
jws_verify_sig_rs_cleanup:
   
   return retval;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_verify_sig_hmac_sha(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   bool retval = false;
   
   // ensure jwk is OCT
   if (jwk->kty != JOSE_JWK_KTY_OCT)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      goto jws_verify_sig_hmac_sha_cleanup;
   }
   
   // verify decrypted digest matches computed digest
   if ((cjose_const_memcmp(jws->dig, jws->sig, jws->dig_len) != 0) || (jws->sig_len != jws->dig_len))
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_verify_sig_hmac_sha_cleanup;
   }
   
   // if we got this far - success
   retval = true;
   
jws_verify_sig_hmac_sha_cleanup:
   
   return retval;
}

////////////////////////////////////////////////////////////////////////////////
static bool jws_verify_sig_ec(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   bool retval = false;
   
   // ensure jwk is EC
   if (jwk->kty != JOSE_JWK_KTY_EC)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   
   ec_keydata *keydata = (ec_keydata *)jwk->keydata;
   EC_KEY *ec = keydata->key;
   
   ECDSA_SIG *ecdsa_sig = ECDSA_SIG_new();
   int key_len = (int)(jws->sig_len / 2);
   
#if (JOSE_OPENSSL_11X)
   BIGNUM *pr = BN_new(), *ps = BN_new();
   BN_bin2bn(jws->sig, key_len, pr);
   BN_bin2bn(jws->sig + key_len, key_len, ps);
   ECDSA_SIG_set0(ecdsa_sig, pr, ps);
#else
   BN_bin2bn(jws->sig, key_len, ecdsa_sig->r);
   BN_bin2bn(jws->sig + key_len, key_len, ecdsa_sig->s);
#endif
   
   if (ECDSA_do_verify(jws->dig, (int)jws->dig_len, ecdsa_sig, ec) != 1)
   {
      JOSE_ERROR(err, JOSE_ERR_CRYPTO);
      goto jws_verify_sig_ec_cleanup;
   }
   
   // if we got this far - success
   retval = true;
   
jws_verify_sig_ec_cleanup:
   if (ecdsa_sig)
      ECDSA_SIG_free(ecdsa_sig);
   
   return retval;
}

////////////////////////////////////////////////////////////////////////////////

bool cjose_jws_verify(cjose_jws_t *jws, const cjose_jwk_t *jwk, jose_err *err)
{
   if (NULL == jws || NULL == jwk)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   
   // build JWS digest from header and payload (hashed signing input value)
   if (!jws->fns.digest(jws, jwk, err))
   {
      return false;
   }
   //TODO fix this
   // verify JWS signature
  // if (! jws->fns.verify(jws, jwk, err))
  // {
  //    return false;
  // }
   
   return true;
}

////////////////////////////////////////////////////////////////////////////////
bool cjose_jws_get_plaintext(const cjose_jws_t *jws, uint8_t **plaintext, size_t *plaintext_len, jose_err *err)
{
   if (NULL == jws || NULL == plaintext || NULL == jws->dat)
   {
      JOSE_ERROR(err, JOSE_ERR_INVALID_ARG);
      return false;
   }
   
   *plaintext = jws->dat;
   *plaintext_len = jws->dat_len;
   
   return true;
}

////////////////////////////////////////////////////////////////////////////////
cJSON *cjose_jws_get_protected(cjose_jws_t *jws)
{
   if (NULL == jws)
   {
      return NULL;
   }
   
   return (cJSON *)jws->hdr;
}
