/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/*! \Environment.h
 *  \brief Headers for Universal Environment manipulation, real env / cJSON struct
 *  \author Created by Danny Goossen on  8/8/18.
 *  \copyright 2018, Danny Goossen. MIT License
 */


#ifndef __oc_runner__Environment__
#define __oc_runner__Environment__

#include <stdio.h>

/**
 * \brief Defines source of Environment
 */
typedef enum {
	
	/** Environment from system env */
	environment_type_env,
	
	/** Environment from cJSON object */
	environment_type_cJSON,
	
} environment_type_t;

#define Environment_setenv(Environment,name,value,overwrite) (Environment->setenv)(Environment,name,value,overwrite)
#define Environment_unsetenv(Environment,name) (Environment->unsetenv)(Environment,name)
#define Environment_getenv(Environment,name) (Environment->getenv)(Environment,name)


/**
 * \brief Typedef Environment
 *
 */
typedef struct Environment_s Environment;

/**
 * \brief Struct Environment
 */
struct Environment_s {

	environment_type_t type; /**< type of environment */
	void * environment; /**< if a not real environment, pointer to the data */
	int (*setenv)(Environment *, const char*,const char*,int ); /**< setenv wrapper */
	int (*unsetenv)(Environment *, const char*); /**< unsetenv wrapper */
	const char * (*getenv)(Environment *, const char*); /**< getenv wrapper */

};

/**
 \brief Initializes an virtual Environment wrapper
 \note Environment does not hold data of the environment!
 */
Environment * Environment_init(void * p,environment_type_t type);

/**
 \brief Clears the Environment wrapper
 \note does not manipulate or free data, only our virtual Environment wrapper

 */
void Environment_clear(Environment**self);




#endif /* defined(__oc_runner__Environment__) */
