//
//  DockerHub.c
//  oc-runner
//
//  Created by Danny Goossen on 19/7/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include "DockerHub.h"
#include "deployd.h"
#include "b64.h"
#include "simple_http.h"



/**
 \brief typedef parameter structure, \b  dkr_parameter_s
 */
typedef  struct hub_parameter_s hub_parameter_t;

/**
 \brief parameter structure, general sub-program settings
 Parameter settings for the dkr API sub programs
 */
struct hub_parameter_s
{
	u_int8_t allow_fail;   /**< if set, on failure, exit code will be 0 */
	char * reference;     /**< tag reference to push image*/
	char * image;         /**< full registry image name */
	char * image_nick;         /**< full registry image name */
	char * creds;         /**< credentials base64 usr/token */
	char * image_description;     /**< the long description as per DockerHub fileref */
	char * image_full_description;     /**< the long description as per DockerHub fileref */
	char * const * pathname; /**< pointer list of pathname to add to layer */
	size_t pathname_cnt; /**< number of pathnames */
	int private;         /**< Set docker registry private */
};

/**
 \brief clear parameters for hub api
 */
void clear_hub_parameters(hub_parameter_t **parameter);

/**
 \brief internal, set's the parameters according to the arguments
 \param argc argument count
 \param argv variable arguments passed from command line in script
 \param parameter docker registry image parameters
 
 */
int process_options_dockerhub_delete_tag(size_t argc, char * const *argv, hub_parameter_t *parameter);
/**
 \brief internal, set's the parameters according to the arguments
 \param argc argument count
 \param argv variable arguments passed from command line in script
 \param parameter docker registry image parameters
 
 */
int process_options_dockerhub_description(size_t argc, char * const *argv, hub_parameter_t *parameter);
/**
 \brief internal, set's the parameters according to the arguments
 \param argc argument count
 \param argv variable arguments passed from command line in script
 \param parameter docker registry image parameters
 
 */
int process_options_dockerhub_delete(size_t argc, char * const *argv, hub_parameter_t *parameter);


// API interface to simple http
/**
 \brief api_data the docker api data structure
 \todo add mutex for registry updates in the database for parallel actions(pulling multiple layers in parallel)
 */
struct hub_api_data_s {
	CURL * curl;              /**< CURL connection */
	char * credential;       /**< the database of the registries */
	char * default_ca_bundle; /**< the default ca_cert bundle */
	char * token;
	int retries;
	//  add mutext for registries
	// individual request data
};


// low level DockerHUB API, interface to simple_http

/**
 * \brief init the docker api
 * \param api_data the data struture is created and initialized and returned
 * \param ca_cert_bundle the default ca_bundle to use
 * \return 0 on success
 * \note apidata should be NULL
 */
int hub_api_init(struct hub_api_data_s ** api_data, const char *ca_cert_bundle, const char * credentials);

/**
 * \brief cleanup the api data
 * \param api_data is *api_data will be NULL when returned
 */
void hub_api_cleanup(struct hub_api_data_s ** api_data);

/**
 \brief api to delete repository on DockerHub
 */
int hub_api_hub_delete_repository(struct hub_api_data_s * api_data, const char *image);
/**
 \brief api to delete tag on DockerHub
 */
int hub_api_hub_delete_tag(struct hub_api_data_s * api_data, const char *image, const char * tag);

/**
 \brief api to set description on DockerHub for a repository
 */
int hub_api_hub_set_description(struct hub_api_data_s * api_data, const char *image,const cJSON * description,int create);

/**
 \brief api to login in set api token
 \returns 0 on success!
 */
int hub_api_get_token (struct hub_api_data_s * api_data);

/**
 \brief api check DockerHub for a repository
 */
int hub_api_hub_check(struct hub_api_data_s * api_data, const char *image);






int DockerHub_set_description(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const *argv)
{
	int res=0;
	struct hub_api_data_s * apidata=NULL;
	
	hub_parameter_t * parameters=calloc(1, sizeof(struct hub_parameter_s));
	cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	int argscount=0;
	const char * docker_env_credentials=cJSON_get_key(env_vars, "DOCKER_CREDENTIALS");
	
	argscount=process_options_dockerhub_description(argc, argv,parameters);
	
	if (!parameters->creds && docker_env_credentials)
		parameters->creds=strdup(docker_env_credentials);
	
	if (argscount < 0 || !parameters->image ||!parameters->creds)
	{
		error(" *** ERRORin command line options, exit \n");
		if (argscount<0)
		{
			Write_dyn_trace(trace, red, "\nError processing option: ");
			Write_dyn_trace(trace, none, "%s\n",optarg);
		}
		else
		{
			Write_dyn_trace(trace, red, "\nError missing options\n");
			if(!parameters->image)
				Write_dyn_trace(trace, cyan, "\n --image=<image> is mandatory\n");
			if(!parameters->creds)
				Write_dyn_trace(trace, cyan, "\n no credentials, set --credentials=base64(\"user:passwd\")\n");
		}
		update_details(trace);
		clear_hub_parameters(&parameters);
		return -1;
	}
	if (argscount!=argc)
		Write_dyn_trace(trace, yellow, "\nIgnoring non option commandline arguments\n");
	
	char * image=process_image_nick(&parameters->image, env_vars, "", "", DOCKER_REGISTRY);
	char * registry_name=NULL;
	char * namespace=NULL;
	char * reference=NULL;
	
	if (!res)chop_image(image, &registry_name, &namespace, &reference);
	
	if (!registry_name || strcmp(registry_name,DOCKER_REGISTRY)!=0 || ! namespace)
	{
		Write_dyn_trace(trace, red, "\nNot a DockerHub image\n");
		update_details(trace);
		clear_hub_parameters(&parameters);
		if(image) free(image);
		if (registry_name) free(registry_name);
		if (namespace) free(namespace);
		if (reference) free(reference);
		return -1;
	}
	else if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
		update_details(trace);
		if(image) free(image);
		if (registry_name) free(registry_name);
		if (namespace) free(namespace);
		if (reference) free(reference);
		clear_hub_parameters(&parameters);
		return -1;
	}
	
	res=hub_api_init(&apidata, DEFAULT_CA_BUNDLE, parameters->creds);
	
	
	cJSON* description=cJSON_CreateObject();
	if (description)
	{
		const char* image_title=NULL;
		if (parameters->image_description && strlen(parameters->image_description)>0)
			
			cJSON_add_string(description, "description", parameters->image_description);
		
		else if ((image_title=cJSON_get_key(env_vars, "OC_INFO_IMAGE_TITLE")) && strlen(image_title)>0)
		
			cJSON_add_string(description, "description",image_title);
		
		else if ((image_title=cJSON_get_key(env_vars, "OC_INFO_IMAGE_REFNAME")) && strlen(image_title)>0 )
		
			cJSON_add_string(description, "description",image_title);
		
		else
		{
			char * tmp=name_space_rev(namespace);
			if (tmp)
			{
				cJSON_add_string(description, "description",tmp);
				free(tmp);
			}
		}
			
		char * filedata=NULL;
		if (parameters->image_full_description)
		{
			cJSON_add_string(description, "image_full_description", parameters->image_full_description);
		}
		else if(cJSON_get_key(env_vars, "OC_INFO_IMAGE_DESCRIPTION"))
			cJSON_add_string_from_object(description, "full_description", env_vars, "OC_INFO_IMAGE_DESCRIPTION");
	    
		else
		{
			filedata=read_a_file_v("%s/description.md",cJSON_get_key(env_vars, "CI_PROJECT_DIR"));
			if (filedata)
			{
				// TODO change this, need command to put string in without copy, this can be big
				cJSON_add_string(description, "full_description", filedata);
				free(filedata);
			}
			else
			{
				filedata=read_a_file_v("%s/README.md",cJSON_get_key(env_vars, "CI_PROJECT_DIR"));
				if (filedata)
				{
					// TODO change this, need command to put string in without copy, this can be big
					cJSON_add_string(description, "full_description", filedata);
					free(filedata);
				}
			}
		}
		
		//100
		image_title=cJSON_get_key(description, "description");
		if (image_title && strlen(image_title)>0)
		{
			cJSON_add_string_n(description, "description", image_title, 100);
			char * t=cJSON_get_stringvalue_as_var(description, "description");
			t[97]='.';
			t[98]='.';
			t[99]='.';
		}
		Write_dyn_trace(trace, none, "\n   Update DockerHub repository: ");
		Write_dyn_trace(trace, cyan, "%s\n",namespace);

		
		// CI_PROJECT_VISIBILITY	10.3	all	The project visibility (internal, private, public)
		if (parameters->private==1)
		{
			Write_dyn_trace(trace, none, "   Setting Repository visibility: ");
			Write_dyn_trace(trace, red, "Private\n");
			cJSON_safe_addBool2Obj(description, "is_private", 1);
		}
		else if(parameters->private==-1)
		{
		
			Write_dyn_trace(trace, none, "   Setting Repository visibility: ");
			Write_dyn_trace(trace, green, "Public\n");
			cJSON_safe_addBool2Obj(description, "is_private", 0);
		}
		else if (parameters->private != 2)
		{
			int is_private=(cJSON_validate_contains_match(env_vars, "CI_PROJECT_VISIBILITY", "private") || cJSON_validate_contains_match(env_vars, "CI_PROJECT_VISIBILITY", "internal")  );
			Write_dyn_trace(trace, none, "   Setting Repository visibility: ");
			if (is_private)
				Write_dyn_trace(trace, red, "Private\n");
			else
				Write_dyn_trace(trace, green, "Public\n");
			cJSON_safe_addBool2Obj(description, "is_private", is_private)
		}
		print_json(description);
		Write_dyn_trace(trace, none,"\n");
		Write_dyn_trace_pad(trace, yellow, 60,"   * Check existence %s",namespace);
		update_details(trace);
		res=hub_api_hub_check(apidata, image);
		
		if (res>=200 && res<300)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			Write_dyn_trace_pad(trace, yellow, 60,"   * Update %s",namespace);
			update_details(trace);
			res=hub_api_hub_set_description(apidata, image, description,0);
		}
		else if (res==404)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			Write_dyn_trace_pad(trace, yellow, 60,"   * Create %s",namespace);
			update_details(trace);
			char* p=strchr(namespace, '/');
			if (p)
			{
				size_t len=p-namespace;
			cJSON_add_string(description, "name", p+1);
			cJSON_add_string_n(description, "namespace", namespace,len);
				print_json(description);
			res=hub_api_hub_set_description(apidata, image, description,1);
			}
			
		}
		if (res>=200 && res<300)
		{
		
			Write_dyn_trace(trace, green, "  [OK]\n");
			res=0;
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n");
			print_api_error(trace, res);
		}
		update_details(trace);

		cJSON_Delete(description);
	}
	else
		Write_dyn_trace(trace, red, "\nSYSTEM Error: Out of memory\n");
	if(image) free(image);
	if (registry_name) free(registry_name);
	if (namespace) free(namespace);
	if (reference) free(reference);
	
	int af= parameters->allow_fail;
	clear_hub_parameters(&parameters);
	
	hub_api_cleanup(&apidata);
	
	if(res && af)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		update_details(trace);
		return 0;
	}
	
	return res;
}

int DockerHub_delete_repository(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const*argv)
{
	int res=0;
	struct hub_api_data_s * apidata=NULL;
	
	hub_parameter_t * parameters=calloc(1, sizeof(struct hub_parameter_s));
	int argscount=0;
	
	cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const char * docker_env_credentials=cJSON_get_key(env_vars, "DOCKER_CREDENTIALS");
	argscount=process_options_dockerhub_delete(argc, argv,parameters);
	
	if (!parameters->creds && docker_env_credentials)
		parameters->creds=strdup(docker_env_credentials);
	
	if (argscount < 0  || !parameters->image || !parameters->creds)
	{
		if (argscount<0)
		{
			Write_dyn_trace(trace, red, "\nError processing option: ");
			Write_dyn_trace(trace, none, "%s\n",optarg);
		}
		else
		{
			Write_dyn_trace(trace, red, "\nError missing options\n");
			if(!parameters->image)
				Write_dyn_trace(trace, cyan, "\n --image=<image> is mandatory\n");
			if(!parameters->creds)
				Write_dyn_trace(trace, cyan, "\n no credentials, set --credentials=base64(\"user:passwd\")\n");
		}
		update_details(trace);
		error(" *** ERRORin command line options, exit \n");
		clear_hub_parameters(&parameters);
		return -1;
	}
	if (argscount!=argc)
		Write_dyn_trace(trace, yellow, "\nIgnoring non option commandline arguments\n");
	
	
	char * image=process_image_nick(&parameters->image, env_vars, "", "", DOCKER_REGISTRY);
	
	char * registry_name=NULL;
	char * namespace=NULL;
	char * reference=NULL;
	
	if (!res && image)chop_image(image, &registry_name, &namespace, &reference);
	
	if (!registry_name || strcmp(registry_name,DOCKER_REGISTRY)!=0 || ! namespace || !image)
	{
		Write_dyn_trace(trace, red, "\nNot a DockerHub image\n");
		update_details(trace);
		clear_hub_parameters(&parameters);
		if(image) free(image);
		if (registry_name) free(registry_name);
		if (namespace) free(namespace);
		if (reference) free(reference);
		return -1;
	}
	else if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
		update_details(trace);
		if(image) free(image);
		if (registry_name) free(registry_name);
		if (namespace) free(namespace);
		if (reference) free(reference);
		clear_hub_parameters(&parameters);
		return -1;
	}
	res= hub_api_init(&apidata, DEFAULT_CA_BUNDLE, parameters->creds);
	if(res)
	{
		Write_dyn_trace(trace, red, "[FAIL]\n");
		Write_dyn_trace(trace, red, "\nSystem Error: ");
		if (res==-1)
			Write_dyn_trace(trace, none, "Out Of Memeory\n");
		else if(res==-1)
			Write_dyn_trace(trace, none, "Failed to get new Curl Handle\n");
		update_details(trace);
	}
	else
	{
		Write_dyn_trace(trace, none, "\n   For Image: ");
		Write_dyn_trace(trace, cyan, "%s\n",namespace);
		Write_dyn_trace_pad(trace, yellow, 60,"   * Check if exist ");
		update_details(trace);
	
	}
	if(!res)
	{
		res=hub_api_hub_check(apidata, image);
		
		if (res==200 && res<300)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			Write_dyn_trace_pad(trace, yellow, 60,"   * Delete: %s",namespace);
			update_details(trace);

			res=hub_api_hub_delete_repository(apidata, image);
		}
		
		
		if (res>=200 && res<300)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			res=0;
		}
		else if (res==404)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			Write_dyn_trace(trace, yellow, "\n Image did not exists\n");
			res=0;
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n");
			print_api_error(trace, res);
		}
	}
	if(image) free(image);
	if (registry_name) free(registry_name);
	if (namespace) free(namespace);
	if (reference) free(reference);

	
	if (apidata)hub_api_cleanup(&apidata);
	if(res && parameters->allow_fail)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		res=0;
	}
	update_details(trace);

	clear_hub_parameters(&parameters);
	return res;
}

int DockerHub_delete_tag(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const *argv)
{
	int res=0;
	struct hub_api_data_s * apidata=NULL;
	
	hub_parameter_t * parameters=calloc(1, sizeof(struct hub_parameter_s));
	int argscount=0;
	cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const char * docker_env_credentials=cJSON_get_key(env_vars, "DOCKER_CREDENTIALS");
	argscount=process_options_dockerhub_delete_tag(argc, argv,parameters);
	
	if (!parameters->creds && docker_env_credentials)
		parameters->creds=strdup(docker_env_credentials);
	
	if (argscount < 0 || !parameters->image || parameters->reference || !parameters->creds)
	{
		if (argscount<0)
		{
			Write_dyn_trace(trace, red, "\nError processing option: ");
			Write_dyn_trace(trace, none, "%s\n",optarg);
		}
		else
		{
			Write_dyn_trace(trace, red, "\nError missing options\n");
			if(!parameters->reference)
				Write_dyn_trace(trace, cyan, "\n --reference=<reference> is mandatory\n");
			if(!parameters->image)
				Write_dyn_trace(trace, cyan, "\n --image=<image> is mandatory\n");
			if(!parameters->creds)
				Write_dyn_trace(trace, cyan, "\n no credentials, set --credentials=base64(\"user:passwd\")\n");
		}
		error(" *** ERRORin command line options, exit \n");
		update_details(trace);
		clear_hub_parameters(&parameters);
		return -1;
	}
	if (argscount!=argc)
		Write_dyn_trace(trace, yellow, "\nIgnoring non option commandline arguments\n");
	

	char * image=process_image_nick(&parameters->image, env_vars, "", "", DOCKER_REGISTRY);
	
	char * registry_name=NULL;
	char * namespace=NULL;
	char * reference=NULL;
	
	if (!res && image)chop_image(image, &registry_name, &namespace, &reference);
	
	if (!registry_name || strcmp(registry_name,DOCKER_REGISTRY)!=0 || ! namespace || !image)
	{
		Write_dyn_trace(trace, red, "\nNot a DockerHub image\n");
		update_details(trace);
		clear_hub_parameters(&parameters);
		if(image) free(image);
		if (registry_name) free(registry_name);
		if (namespace) free(namespace);
		if (reference) free(reference);
		return -1;
	}
	else if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
		update_details(trace);
		if(image) free(image);
		if (registry_name) free(registry_name);
		if (namespace) free(namespace);
		if (reference) free(reference);
		clear_hub_parameters(&parameters);
		return -1;
	}
	res= hub_api_init(&apidata, DEFAULT_CA_BUNDLE, parameters->creds);
	if(res)
	{
		Write_dyn_trace(trace, red, "[FAIL]\n");
		Write_dyn_trace(trace, red, "\nSystem Error: ");
		if (res==-1)
			Write_dyn_trace(trace, none, "Out Of Memeory\n");
		else if(res==-1)
			Write_dyn_trace(trace, none, "Failed to get new Curl Handle\n");
	}
	else
	{
		Write_dyn_trace(trace, none, "\n   For image: ");
		Write_dyn_trace(trace, cyan, "%s\n",namespace);
		Write_dyn_trace_pad(trace, yellow, 60,"   * Delete tag %s",reference);
		update_details(trace);
	}
	if(!res)
	{
		res=hub_api_hub_delete_tag(apidata, image, parameters->reference);
		if (res>=200 && res<300)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			res=0;
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n");
			print_api_error(trace, res);
		}
	}
	if (apidata) hub_api_cleanup(&apidata);
	if(res && parameters->allow_fail)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		res=0;
	}
	update_details(trace);

	clear_hub_parameters(&parameters);
	if(image) free(image);
	if (registry_name) free(registry_name);
	if (namespace) free(namespace);
	if (reference) free(reference);
	return res;
}

int process_options_dockerhub_description(size_t argc, char * const *argv, hub_parameter_t *parameter)
{
	struct option long_options[] = {
		{ "full_description", 1, NULL, 'l' },
		{ "full-description", 1, NULL, 'l' },
		{ "description",      1, NULL, 'd' },
		{ "image",            1, NULL, 'i' },
		{ "credentials",      1, NULL, 'e' },
		{ "allow-fail",       0, NULL, 'f' },
		{ "allow_fail",       0, NULL, 'f' },
		{ "allow-failure",       0, NULL, 'f' },
		{ "allow_failure",       0, NULL, 'f' },
		{ "set-private",      2, NULL, 'p' },
		{ NULL,               0, NULL, 0   }   };
	
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			switch (which) {
				case 'd': {
					parameter->image_description =get_opt_value(optarg, NULL);
				}break;
				case 'l': {
					parameter->image_full_description =get_opt_value(optarg, NULL);
				}break;
				case 'i': {
					parameter->image =get_opt_value(optarg, NULL);
				}break;
				case 'e': {
					parameter->creds =get_opt_value(optarg, NULL);
				}
					break;
				case 'p': {
					if(optarg)
					{
						if (strcmp(optarg,"yes")==0)
							parameter->private = 1;
						else if (strcmp(optarg,"no")==0)
							parameter->private = -1;
						else if (strcmp(optarg,"none")==0)
							parameter->private = -1;
						else parameter->private = 0;
						
					}
					else
					{
						parameter->private = 1;
					}
				}
					break;
				case 'f': {
					parameter->allow_fail=1;
				}
					break;
					/* otherwise    : display usage information */
				default:
					return -1;
					break;
			}
		}
	}
	return optind;
}

int process_options_dockerhub_delete(size_t argc, char * const *argv, hub_parameter_t *parameter)
{
	struct option long_options[] = {
		{ "image",         1, NULL, 'i' },
		{ "allow-fail",       0, NULL, 'f' },
		{ "allow_fail",       0, NULL, 'f' },
		{ "allow-failure",       0, NULL, 'f' },
		{ "allow_failure",       0, NULL, 'f' },		{ "credentials",   1, NULL, 'e' },
		{ NULL,            0, NULL, 0 }
	};
	
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			switch (which) {
				case 'i': {
					parameter->image =calloc(1,strlen(optarg)+1);
					sprintf(parameter->image,"%s",optarg);
				}break;
				case 'f': {
					parameter->allow_fail=1;
				}
					break;
				case 'e': {
					parameter->creds =calloc(1,strlen(optarg)+1);
					sprintf(parameter->creds,"%s",optarg);
				}
					break;
					/* otherwise    : display usage information */
				default:
					return -1;
					break;
			}
		}
	}
	return optind;
}

int process_options_dockerhub_delete_tag(size_t argc, char * const *argv, hub_parameter_t *parameter)
{
	struct option long_options[] = {
		
		{ "reference",     1, NULL, 'r' },
		{ "tag",           1, NULL, 'r' },
		{ "image",         1, NULL, 'i' },
		{ "allow-fail",    0, NULL, 'f' },
		{ "allow_fail",    0, NULL, 'f' },
		{ "allow-failure", 0, NULL, 'f' },
		{ "allow_failure", 0, NULL, 'f' },
		{ "credentials",   1, NULL, 'e' },
		{ NULL,         0, NULL, 0 } };
	
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			/* depending on which option we got */
			switch (which) {
				case 'r': {
					parameter->reference =calloc(1,strlen(optarg)+1);
					sprintf(parameter->reference,"%s",optarg);
				}break;
				case 'i': {
					parameter->image =calloc(1,strlen(optarg)+1);
					sprintf(parameter->image,"%s",optarg);
				}break;
				case 'e': {
					parameter->creds =calloc(1,strlen(optarg)+1);
					sprintf(parameter->creds,"%s",optarg);
				}
					break;
				case 'f': {
					parameter->allow_fail=1;
				}
					break;
					/* otherwise    : display usage information */
				default:
					return-1;
					break;
			}
		}
	}
	return optind;
}

int hub_api_hub_delete_tag(struct hub_api_data_s * api_data, const char *image,const char * tag)
{
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	long res=0;
	chop_image(image, &registry_name, &namespace, &reference);
	cJSON * headers=cJSON_CreateObject();
	if (headers)
	{
		cJSON_add_string_v(headers, "Authorization", "JWT %s",api_data->token);
	}
	res=custom_http_v(api_data->curl, NULL, 0,headers , NULL, 0, NULL, "DELETE", "https://%s/v2/repositories/%s/tags/%s/",registry_name,namespace,tag);
	if (headers) cJSON_Delete(headers);
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);
	return ((int)res);
}

int hub_api_hub_delete_repository(struct hub_api_data_s * api_data, const char *image)
{
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	long res=0;
	chop_image(image, &registry_name, &namespace, &reference);
	cJSON * headers=cJSON_CreateObject();
	if (headers)
	{
		cJSON_add_string_v(headers, "Authorization", "JWT %s",api_data->token);
	}
	res=custom_http_v(api_data->curl, NULL, 0,headers , NULL, 0, NULL, "DELETE", "https://%s/v2/repositories/%s/",registry_name,namespace);
	if (headers) cJSON_Delete(headers);
	
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);

	return ((int)res);
}


int hub_api_get_token (struct hub_api_data_s * api_data)
{
	int res=0;
	unsigned char* up=NULL;
	jose_err err;
	size_t up_len=0;
	char * post_data=NULL;
	
	if (base64_decode(api_data->credential, strlen(api_data->credential), &up, &up_len, &err))
	{
		// {"username": "'${UNAME}'", "password": "'${UPASS}'"}
		// name=daniel&project=curl
		debug("uand p: %s",up);
		if(up)
		{
			char * tok=strchr((char*)up,':');
			int len_n=(int)(tok-(char*)up);
			int lenpw=(int)(up_len-len_n-1);
			cJSON * auth=cJSON_CreateObject();
			cJSON_add_string_n(auth, "username",(char*) up,len_n);
			cJSON_add_string_n(auth, "password", tok+1,lenpw);
			post_data=cJSON_PrintUnformatted(auth);
			free(up);
		}
	}
	
	if(api_data->curl && post_data)
	{
		char * result=NULL;
		cJSON * headers_out=cJSON_CreateObject();
		if (headers_out)
		{
		    cJSON_add_string(headers_out, "Accept", "application/json");
			cJSON_add_string(headers_out, "Content-type", "application/json");
		}
		int count=0;
		if (!api_data->token)
		{
			do{
				if (count) usleep(2000000LL * count*count);
				count++;
				res=custom_http_v(api_data->curl, post_data, 0, headers_out, &result, NULL, NULL, "GET", "https://%s/v2/users/login/",DOCKER_HUB);
			} while (((res>=500 && res<600) || res<0) && count<5);
			if (result)
			{
				cJSON * loginresp=cJSON_Parse(result);
				if (loginresp)
				{
					api_data->token=cJSON_get_stringvalue_dup(loginresp, "token");
					cJSON_Delete(loginresp);
					res=0;
				}
			}
		}
		if (headers_out) cJSON_Delete(headers_out);
	}
	else
		res=CURLE_FAILED_INIT;
	
	return res;
}


int hub_api_hub_check(struct hub_api_data_s * api_data, const char *image)
{
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	int res=0;
	chop_image(image, &registry_name, &namespace, &reference);
	
	if (!api_data->token)
	{
			res=hub_api_get_token(api_data);
	}
	cJSON * headers=cJSON_CreateObject();
	if (headers)
	{
		cJSON_add_string_v(headers, "Authorization", "JWT %s",api_data->token);
		cJSON_add_string_v(headers, "Content-Type", "application/json");
	}
	if (res==0 && api_data->token)
	{
		int count=0;
		do{
			if (count) usleep(2000000LL * count*count);
			count++;
			res=custom_http_v(api_data->curl, NULL, 0,headers , NULL, 0, NULL, "HEAD", "https://%s/v2/repositories/%s/",DOCKER_HUB,namespace);
		} while (((res>=500 && res<600) || res<0) && count<5);
	}
	else res=-1;
	if (headers) cJSON_Delete(headers);
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);
	
	return res;
}

int hub_api_hub_set_description(struct hub_api_data_s * api_data, const char *image,const cJSON * description,int create)
{
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	int res=0;
	chop_image(image, &registry_name, &namespace, &reference);

	//const char accept[]="application/json";
	
	char * data=cJSON_PrintUnformatted(description);
	if (!api_data->token)
	{
			res=hub_api_get_token(api_data);
	}
	if (res==0 && api_data->token)
	{
		cJSON * headers=cJSON_CreateObject();
		if (headers)
		{
			cJSON_add_string_v(headers, "Authorization", "JWT %s",api_data->token);
			cJSON_add_string_v(headers, "Content-Type", "application/json");
		}
		int count=0;
		do{
			if (count) usleep(2000000LL * count*count);
			count++;
			if (create)
				res=custom_http_v(api_data->curl, data, 0,headers , NULL, 0, NULL, "POST", "https://%s/v2/repositories/",DOCKER_HUB);
			else
				res=custom_http_v(api_data->curl, data, 0,headers , NULL, 0, NULL, "PATCH", "https://%s/v2/repositories/%s/",DOCKER_HUB,namespace);
		} while (((res>=500 && res<600) || res<0) && count<5);
		if (headers) cJSON_Delete(headers);
	}
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);
	if (data) free(data);
	return (res);
}

void clear_hub_parameters(hub_parameter_t **parameter)
{
	if (parameter && *parameter)
	{
		if((*parameter)->image_description!=NULL) free((*parameter)->image_description);
		if((*parameter)->image_full_description!=NULL) free((*parameter)->image_full_description);
		if((*parameter)->reference!=NULL) free((*parameter)->reference);
		
		if((*parameter)->creds!=NULL) free((*parameter)->creds);
		if((*parameter)->image_nick!=NULL) free((*parameter)->image_nick);
		free(*parameter);
		(*parameter)=NULL;
	}
	return;
}

int hub_api_init(struct hub_api_data_s ** api_data, const char *ca_cert_bundle ,const char * credentials)
{
	if (*api_data) hub_api_cleanup(api_data);
	
	(*api_data)=calloc(1,sizeof(struct hub_api_data_s));
	if (*api_data)
	{
		(*api_data)->curl =simple_http_init(ca_cert_bundle,easy_http_follow_location,easy_http_quiet, easy_http_SSL_VERIFYPEERL, PACKAGE_STRING);
		(*api_data)->retries=0;
		if ((*api_data)->curl)
		{
			if (ca_cert_bundle)(*api_data)->default_ca_bundle=strdup(ca_cert_bundle);
			if (credentials)(*api_data)->credential=strdup(credentials);
			curl_easy_setopt((*api_data)->curl, CURLOPT_NOPROGRESS, 1L);
			curl_easy_setopt((*api_data)->curl, CURLOPT_TIMEOUT, 60L); // 1min
			curl_easy_setopt((*api_data)->curl, CURLOPT_CONNECTTIMEOUT,30L); // 30 sec on connection
			return 0;
		}
		else
		{
			hub_api_cleanup(api_data);
			return -2;
		}
	}
	else return -1;
}

void hub_api_cleanup(struct hub_api_data_s ** api_data)
{
	if (*api_data)
	{
		curl_easy_setopt((*api_data)->curl, CURLOPT_CAINFO, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_READDATA,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_WRITEHEADER, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_WRITEDATA, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_WRITEFUNCTION, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_HEADERFUNCTION,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_READDATA, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_UPLOAD, 0L);
		curl_easy_setopt((*api_data)->curl, CURLOPT_INFILESIZE_LARGE,0L);
		curl_easy_setopt((*api_data)->curl, CURLOPT_POSTFIELDS,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_READFUNCTION, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_CUSTOMREQUEST,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_URL,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_HTTPHEADER, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_PROGRESSDATA, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_PROGRESSFUNCTION, NULL);
		
		if ((*api_data)->credential) free((*api_data)->credential);
		if ((*api_data)->token) free((*api_data)->token);
		if ((*api_data)->default_ca_bundle) free((*api_data)->default_ca_bundle);
		if ((*api_data)->curl) curl_easy_cleanup((*api_data)->curl);
		(*api_data)->curl=NULL;
		(*api_data)->credential=NULL;
		(*api_data)->default_ca_bundle=NULL;
		//debug("Cleanup *api_data\n");
		free(*api_data);
		debug("Done Cleanup *api_data\n");
		*api_data=NULL;
	}
	return ;
}

