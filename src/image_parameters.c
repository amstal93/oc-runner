/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/**
 * \file image_parameters.c
 * \brief parameter handling image for registry/Imagestream etc
 * \author Created by Danny Goossen on  9/8/18.
 * \copyright 2018, Danny Goossen. MIT License
 */

#include "deployd.h"
#include <getopt.h>
#include "image_parameters.h"

/**
 \brief internal
 \param env_vars environment variables defined per ci-job in struct cJSON
 \param parameter docker registry image parameters
 */
static void set_missing_parms_push(cJSON * env_vars,dkr_parameter_t *parameter);

void clear_dkr_parameters(dkr_parameter_t **parameter)
{
	if (parameter && *parameter)
	{
		if((*parameter)->info_command!=NULL) free((*parameter)->info_command);
		if((*parameter)->rootfs!=NULL) free((*parameter)->rootfs);
		if((*parameter)->image_name!=NULL) free((*parameter)->image_name);
		if((*parameter)->image_description!=NULL) free((*parameter)->image_description);
		if((*parameter)->image_long_description_filename!=NULL) free((*parameter)->image_long_description_filename);
		if((*parameter)->reference!=NULL) free((*parameter)->reference);
		if((*parameter)->image_dir!=NULL) free((*parameter)->image_dir);
		if((*parameter)->image!=NULL) free((*parameter)->image);
		if((*parameter)->registry_name!=NULL) free((*parameter)->registry_name);
		if((*parameter)->namespace!=NULL) free((*parameter)->namespace);
		if((*parameter)->tag!=NULL) free((*parameter)->tag);
		if((*parameter)->creds!=NULL) free((*parameter)->creds);
		if((*parameter)->image_nick!=NULL) free((*parameter)->image_nick);
		if((*parameter)->archive!=NULL) free((*parameter)->archive);
		if((*parameter)->layer_blob_digest!=NULL) free((*parameter)->layer_blob_digest);
		if((*parameter)->layer_content_digest!=NULL) free((*parameter)->layer_content_digest);
		
		if((*parameter)->docker_config_digest!=NULL) free((*parameter)->docker_config_digest);
		
		free(*parameter);
		(*parameter)=NULL;
	}
	return;
}

//TODO: have a parameter wich options are allowed
int process_options(size_t argc, char *const*argv, dkr_parameter_t *parameter)
{
	
	struct option long_options[] = {
		{ "verbose",       0, NULL, 'v' },
		{ "quiet",         0, NULL, 'q' },
		{ "u2g",           0, NULL, 'u' },
		{ "rootfs",        1, NULL, 'p' },
		{ "layer",         1, NULL, 'l' },
		{ "reference",     1, NULL, 'r' },
		{ "tag",           1, NULL, 't' },
		{ "image_dir",     1, NULL, 'i' },
		{ "image-dir",     1, NULL, 'i' },
		{ "name",          1, NULL, 'n' },
		{ "allow_fail",    0, NULL, 'f' },
		{ "allow-fail",    0, NULL, 'f' },
		{ "allow-failure", 0, NULL, 'f' },
		{ "allow_failure", 0, NULL, 'f' },
		{ NULL,         0, NULL, 0 } };
	
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameter->quiet)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					{
						parameter->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameter->verbose)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
						parameter->quiet = 1;
				}
					break;
					
					/* --rootfs         : path to rootfs to transform */
				case 'u': {
					parameter->u2g=1;
				}
					break;
				case 'p': {
					parameter->rootfs =calloc(1,strlen(optarg)+1);
					sprintf(parameter->rootfs,"%s",optarg);
				}
					break;
				case 'l': {
					parameter->archive =calloc(1,strlen(optarg)+1);
					sprintf(parameter->archive,"%s",optarg);
				}
					break;
				case 'r': {
					parameter->reference =calloc(1,strlen(optarg)+1);
					sprintf(parameter->reference,"%s",optarg);
				}
					break;
				case 'i': {
					parameter->image_dir =calloc(1,strlen(optarg)+1);
					sprintf(parameter->image_dir,"%s",optarg);
				}
					break;
				case 'n': {
					parameter->image_name =calloc(1,strlen(optarg)+1);
					sprintf(parameter->image_name,"%s",optarg);
				}
			 case 't': {
				 parameter->tag =calloc(1,strlen(optarg)+1);
				 sprintf(parameter->tag,"%s",optarg);
			 }break;
				case 'f': {
					parameter->allow_fail=1;
				}
					break;
				case 'V':
				{
					error("%s\n",GITVERSION);
					exit(0);
				}
					/* otherwise    : display usage information */
				default:
					;
					break;
			}
		}
	}
	return optind;
}

int prep_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data)
{
	
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const char * project_dir=cJSON_get_key(env_vars,"CI_PROJECT_DIR");
	
	debug("working in %s\n",project_dir);
	
	int exit_code=0;
	
	if (parameters->ISR)
	{
		set_ISR_registry(job, parameters, api_data,oc_api_data);
	}
	else if (parameters->GLR)
	{
		if (!cJSON_get_key(env_vars, "CI_REGISTRY_IMAGE")) exit_code=-1;
		if (!exit_code)
			set_GLR_registry(job, parameters, api_data);
	}
	else exit_code=set_registry(job, parameters, api_data, oc_api_data);
	
	
	return( exit_code);
}

int set_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data)
{
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"OC-RUNNER-IMAGESTRAM-IP");
	int IMAGESTRAM_Insecure=cJSON_safe_IsTrue(sys_vars,"OC-RUNNER-IMAGESTRAM-Insecure");
	const char * OC_NAMESPACE=cJSON_get_key(sys_vars,"OC-RUNNER-OC-NAMESPACE");
	const char * token= cJSON_get_key(sys_vars,"OC-RUNNER-TOKEN");
	const char * docker_registry_domain=cJSON_get_key(sys_vars,"OC-RUNNER-DOCKER-REGISTRY");
	const char * master_url=cJSON_get_key(sys_vars,"OC-MASTER-URL");
	const char * docker_creds=cJSON_get_key(env_vars, "DOCKER_CREDENTIALS");
	
	if (parameters->image)
	{
		debug("set registry\n");
		if (!*api_data) dkr_api_init(api_data, DEFAULT_CA_BUNDLE);
		parameters->image_nick=parameters->image;
		parameters->image=NULL;
		debug("processing image_nick: %s \n",parameters->image_nick);
		parameters->image=process_image_nick(&parameters->image_nick, env_vars, ImageStream_domain, OC_NAMESPACE, docker_registry_domain);
		
		//TODO
		if (parameters->image)
		{
			debug("chop image %s \n",parameters->image);
			chop_image(parameters->image,&parameters->registry_name,&parameters->namespace,&parameters->reference);
			// need something, token gitlab / token imagestream
			if (strcmp(parameters->registry_name,ImageStream_domain)==0)
			{
				if (parameters->creds)
					dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace,NULL,NULL, parameters->creds,IMAGESTRAM_Insecure);
				else
					dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace,"",token, NULL,IMAGESTRAM_Insecure);
				parameters->ISR=1;
			}
			else if (cJSON_get_key(env_vars, "CI_REGISTRY") && strcmp(parameters->registry_name,cJSON_get_key(env_vars, "CI_REGISTRY"))==0)
			{
				if (parameters->creds)
					dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace ,NULL,NULL, parameters->creds,0);
				else
					dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace ,cJSON_get_key(env_vars,"CI_REGISTRY_USER" ), cJSON_get_key(env_vars,"CI_REGISTRY_PASSWORD" ), NULL,0);
				parameters->GLR=1;
			}
			else
			{
				if (parameters->creds)
					dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace, NULL,NULL, parameters->creds,0);
				else if (strcmp(parameters->registry_name,DOCKER_REGISTRY)==0 && docker_creds)
				{
					parameters->creds=strdup(docker_creds);
					dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace, NULL,NULL, parameters->creds,0);
				}
				else
					dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace,NULL,NULL, NULL,0);			}
			
			if (strcmp(parameters->registry_name,ImageStream_domain)==0 && oc_api_data && !*oc_api_data)
			{
				oc_api_init(oc_api_data, DEFAULT_CA_BUNDLE, master_url, token);
			}
			
			return 0;
		}
		else
		{
			debug("no image after processing nick\n");
			return -1;
		}
	}
	else
	{
		debug("no image to work with\n");
		return -1;
	}
}

int set_registry_by_image(const cJSON * job, const char * image,struct dkr_api_data_s * api_data)
{
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"OC-RUNNER-IMAGESTRAM-IP");
	int IMAGESTRAM_Insecure=cJSON_safe_IsTrue(sys_vars,"OC-RUNNER-IMAGESTRAM-Insecure");
	const char * OC_NAMESPACE=cJSON_get_key(sys_vars,"OC-RUNNER-OC-NAMESPACE");
	const char * token= cJSON_get_key(sys_vars,"OC-RUNNER-TOKEN");
	const char * docker_registry_domain=cJSON_get_key(sys_vars,"OC-RUNNER-DOCKER-REGISTRY");
	const char * docker_creds=cJSON_get_key(env_vars, "DOCKER_CREDENTIALS");
	dkr_parameter_t * parameters=calloc(1, sizeof(dkr_parameter_t));
	parameters->image=strdup(image);
	if (parameters->image)
	{
		debug("set registry\n");
		parameters->image_nick=parameters->image;
		parameters->image=NULL;
		debug("processing image_nick: %s \n",parameters->image_nick);
		parameters->image=process_image_nick(&parameters->image_nick, env_vars, ImageStream_domain, OC_NAMESPACE, docker_registry_domain);
		
		//TODO
		if (parameters->image)
		{
			debug("chop image %s \n",parameters->image);
			chop_image(parameters->image,&parameters->registry_name,&parameters->namespace,&parameters->reference);
			// need something, token gitlab / token imagestream
			if (strcmp(parameters->registry_name,ImageStream_domain)==0)
			{
				if (parameters->creds)
					dkr_api_add_registry(api_data, parameters->registry_name,parameters->namespace,NULL,NULL, parameters->creds,IMAGESTRAM_Insecure);
				else
					dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace,"",token, NULL,IMAGESTRAM_Insecure);
				parameters->ISR=1;
			}
			else if (cJSON_get_key(env_vars, "CI_REGISTRY") && strcmp(parameters->registry_name,cJSON_get_key(env_vars, "CI_REGISTRY"))==0)
			{
				if (parameters->creds)
					dkr_api_add_registry(api_data, parameters->registry_name,parameters->namespace ,NULL,NULL, parameters->creds,0);
				else
					dkr_api_add_registry(api_data, parameters->registry_name,parameters->namespace ,cJSON_get_key(env_vars,"CI_REGISTRY_USER" ), cJSON_get_key(env_vars,"CI_REGISTRY_PASSWORD" ), NULL,0);
				parameters->GLR=1;
			}
			else
			{
				if (parameters->creds)
					dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace, NULL,NULL, parameters->creds,0);
				else if (strcmp(parameters->registry_name,DOCKER_REGISTRY)==0 && docker_creds)
					dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace, NULL,NULL, docker_creds,0);
				else
					dkr_api_add_registry(api_data, parameters->registry_name, parameters->namespace,NULL,NULL, NULL,0);
			}
			
			clear_dkr_parameters(&parameters);
			return 0;
		}
		else
		{
			clear_dkr_parameters(&parameters);
			debug("no image after processing nick\n");
			return -1;
		}
	}
	else
	{
		clear_dkr_parameters(&parameters);
		debug("no image to work with\n");
		return -1;
	}
}



void set_missing_parms_push(cJSON * env_vars,dkr_parameter_t *parameter)
{
	// we should check env
	
	if(parameter->archive==NULL) {
		parameter->archive =calloc(1,strlen("layer.tar.gz")+1);
		sprintf(parameter->archive,"layer.tar.gz");
	}
	if(parameter->image_dir==NULL) {
		parameter->image_dir =calloc(1,strlen(cJSON_get_key(env_vars, "CI_PROJECT_DIR"))+strlen("/image")+1);
		sprintf(parameter->image_dir,"%s/image",cJSON_get_key(env_vars, "CI_PROJECT_DIR"));
	}
	if (! parameter->verbose && !parameter->quiet) parameter->verbose = VERBOSE_YN;
	return;
}




int prep_GLR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,size_t argc,char*const*argv)
{
	if ((optind=process_options(argc, argv,parameters)) < 0)
	{
		error(" *** ERRORin command line options, exit \n");
		return (-1);
	}
	if (argc > 1 && optind <= argc) {
		alert("ignoring non option arguments\n");
	}
	set_missing_parms_push(	cJSON_GetObjectItem(job ,"env_vars"),parameters);
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	if (!cJSON_get_key(env_vars,"CI_REGISTRY_IMAGE")) return -1;
	set_GLR_registry(job, parameters, api_data);
	return 0;
}

void set_GLR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data)
{
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"OC-RUNNER-IMAGESTRAM-IP");
	const char * OC_NAMESPACE=cJSON_get_key(sys_vars,"OC-RUNNER-OC-NAMESPACE");
	
	
	size_t nick_len=strlen("Gitlab")  + 1;
	if (parameters->image_name)
		nick_len+=strlen(parameters->image_name)  + 1;
	if (parameters->reference)
		nick_len+=strlen(parameters->reference)  + 1;
	parameters->image_nick=calloc(1,nick_len);
	
	sprintf(parameters->image_nick, "Gitlab");
	if (parameters->image_name)
		sprintf(parameters->image_nick, "%s/%s",parameters->image_nick,parameters->image_name);
	
	if(parameters->reference)
	{
		sprintf(&parameters->image_nick[strlen(parameters->image_nick)], ":%s",parameters->reference);
	}
	
	
	if (parameters->image) free(parameters->image);
	parameters->image=NULL;
	debug("set GLR, before processing image_nick: %s\n",parameters->image_nick);
	parameters->image=process_image_nick(&parameters->image_nick, env_vars, ImageStream_domain,OC_NAMESPACE , DOCKER_REGISTRY);
	debug("set GLR image_nick: %s, image:%s\n",parameters->image_nick,parameters->image);
	chop_image(parameters->image,&parameters->registry_name,&parameters->namespace,&parameters->reference);
	
	debug("set GLR,chop rg: %s, ns: %s, rf: %s\n",parameters->registry_name,parameters->namespace,parameters->reference);
	
	// prepare api
	
	if (!(*api_data)) dkr_api_init(api_data, DEFAULT_CA_BUNDLE);
	
	if (parameters->creds)
		dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace ,NULL,NULL, parameters->creds,0);
	else
		dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace ,cJSON_get_key(env_vars,"CI_REGISTRY_USER" ), cJSON_get_key(env_vars,"CI_REGISTRY_PASSWORD" ), NULL,0);
	
	alert("added registry %s to database\n",parameters->registry_name);
	return;
}

void set_ISR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data)
{
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const char* ImageStream_domain=cJSON_get_key(sys_vars,"OC-RUNNER-IMAGESTRAM-IP");
	int IMAGESTRAM_Insecure=cJSON_safe_IsTrue(sys_vars,"OC-RUNNER-IMAGESTRAM-Insecure");
	const char * OC_NAMESPACE=cJSON_get_key(sys_vars,"OC-RUNNER-OC-NAMESPACE");
	const char * token= cJSON_get_key(sys_vars,"OC-RUNNER-TOKEN");
	const char * master_url= cJSON_get_key(sys_vars,"OC-MASTER-URL");
	debug("set ImageStream registry\n");
	
	size_t nick_len=strlen("ImageStream")  + 1;
	if (parameters->image_name)
		nick_len=nick_len+strlen(parameters->image_name)  + 1;
	if (parameters->reference)
		nick_len=nick_len+strlen(parameters->reference)  + 1;
	
	parameters->image_nick=calloc(1,nick_len);
	
	sprintf(parameters->image_nick, "ImageStream");
	if (parameters->image_name)
		sprintf(parameters->image_nick+strlen(parameters->image_nick), "/%s",parameters->image_name);
	if(parameters->reference)
	{
		sprintf(parameters->image_nick+strlen(parameters->image_nick), ":%s",parameters->reference);
	}
	
	if (!*api_data) dkr_api_init(api_data, DEFAULT_CA_BUNDLE);
	
	if (parameters->image) free(parameters->image);
	parameters->image=process_image_nick(&parameters->image_nick, env_vars, ImageStream_domain,OC_NAMESPACE , DOCKER_REGISTRY);
	chop_image(parameters->image,&parameters->registry_name,&parameters->namespace,&parameters->reference);
	
	if (parameters->creds)
		dkr_api_add_registry(*api_data, parameters->registry_name,parameters->namespace,NULL,NULL, parameters->creds,IMAGESTRAM_Insecure);
	else
		dkr_api_add_registry(*api_data, parameters->registry_name, parameters->namespace,"",token, NULL,IMAGESTRAM_Insecure);
	alert("added registry %s to database\n",parameters->registry_name);
	
	
	
	if (oc_api_data && !*oc_api_data) oc_api_init(oc_api_data, DEFAULT_CA_BUNDLE, master_url, token);
	
	return;
}
