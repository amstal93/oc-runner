//
//  c-ares.c
//  oc-runner
//
//  Created by Danny Goossen on 27/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include "c-ares.h"

#include <ares.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include "error.h"

static void
state_cb(__attribute__((unused))void *data, int s, int read, int write)
{
   debug("Change state fd %d read:%d write:%d\n", s, read, write);
}


static void
callback(void *arg, int status, __attribute__((unused))int timeouts, struct hostent *host)
{
   
   if(!host || status != ARES_SUCCESS){
      error("Failed to lookup %s\n", ares_strerror(status));
      return;
   }
   
   //printf("Found address name %s\n", host->h_name);
   char ip[INET6_ADDRSTRLEN+1];
   bzero(ip, INET6_ADDRSTRLEN+1);
   inet_ntop(host->h_addrtype, host->h_addr_list[0], ip, sizeof(ip));
   if (arg && ip && strlen(ip)>0) *((void **)arg)=strdup(ip);
   /*
   for (i = 0; host->h_addr_list[i]; ++i) {
      inet_ntop(host->h_addrtype, host->h_addr_list[i], ip, sizeof(ip));
      printf("%s\n", ip);
   }
    */
   
}
static void
callback_host(void *arg, int status, __attribute__((unused))int timeouts, struct hostent *host)
{
   
   if(!host || status != ARES_SUCCESS){
      error("Failed to lookup %s\n", ares_strerror(status));
      return;
   }
   if (arg && host->h_name && strlen(host->h_name)>0) *((void **)arg)=strdup(host->h_name);
   
}
static void
wait_ares(ares_channel channel)
{
   for(;;){
      struct timeval *tvp, tv;
      fd_set read_fds, write_fds;
      int nfds;
      
      FD_ZERO(&read_fds);
      FD_ZERO(&write_fds);
      nfds = ares_fds(channel, &read_fds, &write_fds);
      if(nfds == 0){
         break;
      }
      tvp = ares_timeout(channel, NULL, &tv);
      select(nfds, &read_fds, &write_fds, NULL, tvp);
      ares_process(channel, &read_fds, &write_fds);
   }
}

char * getip(const char * hostname)
{
   ares_channel channel;
   int status;
   struct ares_options options;
   int optmask = 0;
   /* is initialized by curl
   if (ARES_SUCCESS!=ares_library_initialized())
   {
      status = ares_library_init(ARES_LIB_INIT_ALL);
      if (status != ARES_SUCCESS){
      printf("ares_library_init: %s\n", ares_strerror(status));
      return NULL;
      }
   }
    */
   //options.sock_state_cb_data;
   options.sock_state_cb = state_cb;
   optmask |= ARES_OPT_SOCK_STATE_CB;
   
   status = ares_init_options(&channel, &options, optmask);
   if(status != ARES_SUCCESS) {
      error("ares_init_options: %s\n", ares_strerror(status));
      return NULL;
   }
   char * result=NULL;
   ares_gethostbyname(channel, hostname, AF_INET, callback, &result);
   //ares_gethostbyname(channel, "google.com", AF_INET6, callback, NULL);
   wait_ares(channel);
   ares_destroy(channel);
   debug("Resolved host %s => %s\n",hostname,result);
   return result;
}

char * gethost(const char * hostip)
{
   ares_channel channel;
   int status;
   struct ares_options options;
   int optmask = 0;
   /*
   if (ARES_SUCCESS!=ares_library_initialized())
   {
      status = ares_library_init(ARES_LIB_INIT_ALL);
      if (status != ARES_SUCCESS){
         printf("ares_library_init: %s\n", ares_strerror(status));
         return NULL;
      }
   }
    */
   //options.sock_state_cb_data;
   options.sock_state_cb = state_cb;
   optmask |= ARES_OPT_SOCK_STATE_CB;
   
   status = ares_init_options(&channel, &options, optmask);
   if(status != ARES_SUCCESS) {
      error("ares_init_options: %s\n", ares_strerror(status));
      return NULL;
   }
   char * result=NULL;
   struct in_addr ip;
   inet_aton(hostip, &ip);
   
   ares_gethostbyaddr(channel, &ip, sizeof ip ,AF_INET, callback_host, &result);

   wait_ares(channel);
   ares_destroy(channel);
   debug("Resolved adress %s => %s\n",hostip,result);
   return result;
}

