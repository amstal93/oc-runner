/*! \file downloads.h
 *  \brief Header file for downloads
 *  \author Created by Danny Goossen on 23/7/17.
 *  \copyright Copyright (c) 2017 Danny Goossen. All rights reserved.
 */

#ifndef __deploy_runner__downloads__
#define __deploy_runner__downloads__

#include "common.h"
/*! \def NUMT
 *  \brief Maximun number of threads used for parallel artifacts downloads
 */
#define NUMT 6




/*! \struct dl_artifacts_s
    \brief Struct used for exchange of data between threads in downloads
*/
struct dl_artifacts_s {
   struct data_thread data[NUMT]; /**< Array of thread data structures, one for each thread */
   int free_count;                /**< Number of free threads (\b NUMT - used threads )*/
   int todo;                      /**< How many downloads sill to do */
   int next;                      /**< next item to download, -1 if finished */
   int next_unzip;                /**< next item to unzip, -1 if finished */
   int unzip_todo;                /**< How many unzips sill to do */
   pthread_t tid[NUMT];           /**< Array with the thread pid's */
   char * proj_dir;               /**< directory where to unzip the downloads */
   char * zip_dir;                /**< directory where to download tempory zip's */

};

/*! @fn cJSON * prepare_artifacts(cJSON * job)
    @brief Prepare and Creates a cJSON struct from \a job:artifacts[]:
    Prepare artifacts create a JSON structure containing everything needed to download artifact
    @param job Gitlab ci Job 
    @return cJSON struct with the artifacts and tokens
 */
cJSON * prepare_artifacts(cJSON * job);

void scan_artifacts_status(cJSON * artifacts, struct trace_Struct *trace);

/*! @fn  init_download_artifacts(struct dl_artifacts_s * dl_artifacts, int count,char * proj_dir, char* zip_dir, const char * ca_bundle);
 *  @brief Initialize the artifacts downloads
 *  init and have a struct pointer with thread info
 *  @param dl_artifacts provide with clean struct
 *  @param count the number of threads
 *  @param proj_dir the project directory where to unzip the downloads
 *  @param zip_dir tempory directory to download the zip files
 *  @param ca_bundle for https connections
 *
 *  @note Use close_download_artifacts() to free memory
 */
void init_download_artifacts(struct dl_artifacts_s * dl_artifacts, int count,char * proj_dir, char* zip_dir, const char * ca_bundle);

/*! @fn void close_download_artifacts(struct dl_artifacts_s * dl_artifacts)
 *  @brief free's all memory and cleans up
 *  Cleanup the stuct
 *  @param dl_artifacts where all data is stored
 */
void close_download_artifacts(struct dl_artifacts_s * dl_artifacts);

/**
 *  @brief Cancels all downloads
 *
 *  @param dl_artifacts where all data is stored
 */
void cancel_download_artifacts(struct dl_artifacts_s * dl_artifacts);

/*! @fn  int download_artifacts(cJSON * artifacts, struct dl_artifacts_s * dl_artifacts)
 *  @brief downloads the \a artifacts
 *  This function is re-entrant called, 
 *   when needed it will start new thread for a new download or unzips a file
 *
 *  @param artifacts the json structure with artifacts to download
 *  @param dl_artifacts where all data is stored
 *  @return returns the number of actions to do, -1 on error
 *  @note Re-entrant fuction
 *
 */
int download_artifacts(cJSON * artifacts, struct dl_artifacts_s * dl_artifacts);

/*! @fn  int progress_change(struct dl_artifacts_s * dl_artifacts)
 *  @brief quick scan of the threads to see if ANY PROGRESS CHANGE
 *
 *  @param dl_artifacts where all data is stored
 *  @return returns 1 if there was a change in progress
 *  @note used to see if we can update feedback to client
 */
int progress_change(struct dl_artifacts_s * dl_artifacts);

#endif /* defined(__deploy_runner__downloads__) */
