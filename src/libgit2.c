//
//  libgit2.c
//  deployctl
//
//  Created by Danny Goossen on 19/8/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "deployd.h"
#include "libgit2.h"

struct dl_data {
   git_remote *remote;
   git_fetch_options *fetch_opts;
   int ret;
   int finished;
};

struct payload_t{
   const char * hash;
   const char * ref;
   char * msg;
   char * msg_param;
   struct trace_Struct * trace;
   int * count_progress;
};



static int fetch_sideband_progress_cb(const char *str, int len, void *data)
{
   struct trace_Struct * trace=((struct payload_t *)data)->trace;
   int * count_progress=((struct payload_t *)data)->count_progress;
   (*count_progress)++;
  // hex_dump_msg(str,len,"Rdump %d\n",*count_progress);
   clear_till_mark_dynamic_trace(trace);
   if (len >0 && (str[len-1]=='\r'|| str[len-1]=='\n' ))
       Write_dyn_trace(trace, none,"    remote: %.*s\n", len-1, str);
   else if (len >0)
      Write_dyn_trace(trace, none,"    remote: %.*s", len, str);
   if (len > 0 && str[len-1]==0x0a)
   {
      //set_mark_dynamic_trace(trace);
      update_details(trace);
      debug("clear count progress from %d\n",*count_progress);
      (*count_progress)=0;
   }
   else if (len > 0 && (*count_progress)%20==0)
   {
      update_details(trace);
      debug("Update detail count %d\n",*count_progress);
   }
   fflush(stdout); /* We don't have the \n to force the flush */
   return 0;
}

/**
 * This function gets called for each remote-tracking branch that gets
 * updated. The message we output depends on whether it's a new one or
 * an update.
 */
static int update_tips_cb(const char *refname, const git_oid *a, const git_oid *b, void *data)
{
   char a_str[GIT_OID_HEXSZ+1], b_str[GIT_OID_HEXSZ+1];
   struct trace_Struct * trace=((struct payload_t *)data)->trace;
   const char * ref =((struct payload_t *)data)->ref;
   git_oid_fmt(b_str, b);
   b_str[GIT_OID_HEXSZ] = '\0';

   if (strncmp(b_str, ((struct payload_t *)data)->hash, 20)==0 || (strstr(refname, ref)!= 0 ) )
   {

      if (git_oid_iszero(a)) {
         clear_till_mark_dynamic_trace(trace);
         Write_dyn_trace(trace, cyan,"    new [%s] => #%.8s: %s\n", ref, b_str,refname);
         set_mark_dynamic_trace(trace);
         update_details(trace);
      } else {
         git_oid_fmt(a_str, a);
         a_str[GIT_OID_HEXSZ] = '\0';
         alert("[updated] %.10s..%.10s %s\n", a_str, b_str, refname);
      }
   }
   return 0;
}

/**
 * This gets called during the download and indexing. Here we show
 * processed and total objects in the pack and the amount of received
 * data. Most frontends will probably want to show a percentage and
 * the download rate.
 */
static int transfer_progress_cb(const git_transfer_progress *stats, void *payload)
{
   struct trace_Struct * trace=((struct payload_t *)payload)->trace;
   clear_till_mark_dynamic_trace(trace);
   if (stats->received_objects == stats->total_objects)
   {
       Write_dyn_trace(trace, none,"    Resolving deltas %d/%d\n",stats->indexed_deltas, stats->total_deltas);

      //if ( stats->indexed_deltas && stats->indexed_deltas==stats->total_deltas) set_mark_dynamic_trace(trace);
      if (stats->indexed_deltas % 100==0 || stats->indexed_deltas==stats->total_deltas) update_details(trace);
   }
   else if (stats->total_objects > 0)
   {
      set_mark_dynamic_trace(trace);
       Write_dyn_trace(trace, none,"    Received %d/%d objects (%d) in %zu bytes\n",stats->received_objects, stats->total_objects,stats->indexed_objects, stats->received_bytes);
       if (stats->received_objects && stats->received_objects==stats->total_objects) set_mark_dynamic_trace(trace);
       if (stats->received_objects % 1000 ==0 || stats->received_objects==stats->total_objects) update_details(trace);
   }
   //else if (stats->total_objects == 0)
  //    set_mark_dynamic_trace(trace);

   return 0;
}

static void checkout_progress(
                       __attribute__((unused))const char *path,
                       size_t completed_steps,
                       size_t total_steps,
                       void *payload)
{
   char * msg=((struct payload_t *)payload)->msg;
   char * msg_param=((struct payload_t *)payload)->msg_param;
   struct trace_Struct * trace=((struct payload_t *)payload)->trace;
   clear_till_mark_dynamic_trace(trace);
   Write_dyn_trace(trace, none,"    %s%s (%zu/%zu)\n",msg,msg_param, completed_steps,total_steps);

   if (completed_steps && completed_steps==total_steps) set_mark_dynamic_trace(trace);
   if (completed_steps%50==0 || completed_steps==total_steps) update_details(trace);
}

static int use_remote(git_repository *repo, const char * repo_url,const char *ref, const char *ref_type,const char* sha,struct trace_Struct * trace)
{
   git_remote *remote = NULL;
   int result;
   const git_remote_head **refs;
   size_t refs_len, i;
   char short_sha[9];
	
	int remote_objects=0;
	
   char newhead_ref[1024];
   // Payload for callbacks
   struct payload_t payload;
   payload.hash=sha;
   payload.trace=trace; // trace buffer
   payload.ref=ref;
   int count_progress=0;
   payload.count_progress=&count_progress; // to not overload trace

   sprintf(short_sha,"%.8s", sha);

   sprintf(newhead_ref,"refs/heads/%s",ref);
   if (strcmp(ref_type,"tag")==0)
      sprintf(newhead_ref, "refs/heads/master");
   else
      sprintf(newhead_ref, "refs/heads/%s",ref);

   //Find the remote by name
   result = git_remote_lookup(&remote, repo, repo_url);
   if (result < 0) {
      if (result!=GIT_ENOTFOUND)
         result = git_remote_create_anonymous(&remote, repo, repo_url);
      if (result < 0)
         goto cleanup;
   }

   //Connect to the remote and call the printing function for each of the remote references.

   result = git_remote_connect(remote, GIT_DIRECTION_FETCH, NULL, NULL, NULL);
   if (result < 0)
      goto cleanup;

   //Get the list of references on the remote and print out their name next to what they point to.
   int foundrefspec=0;
   char newrefspec[255];
   if (git_remote_ls(&refs, &refs_len, remote) < 0)
      goto cleanup;

   for (i = 0; i < refs_len; i++) {
      char oid[GIT_OID_HEXSZ + 1] = {0};
      git_oid_fmt(oid, &refs[i]->oid);
      //printf("%s\t%s\n", oid, refs[i]->name);
      if (strcmp(oid,sha)==0)
      {
         Write_dyn_trace(trace, yellow, "  + Quick Fetch %s \n",refs[i]->name);
         update_details(trace);
         sprintf(newrefspec, "%s:%s",refs[i]->name,newhead_ref);
         foundrefspec=1;
         break;
      }
   }

   if (!foundrefspec)
   {
      Write_dyn_trace(trace, yellow, "  + Fetch %s %s\n",ref_type,ref);
      update_details(trace);
      if (strcmp(ref_type,"tag")==0)
         sprintf(newrefspec, "refs/tags/%s:%s",ref,newhead_ref);
      else
         sprintf(newrefspec, "refs/heads/%s:%s",ref,newhead_ref);
   }
   const git_transfer_progress *stats;
#ifdef CLANG
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wmissing-field-initializers"
#endif
   git_fetch_options fetch_opts = GIT_FETCH_OPTIONS_INIT;
#ifdef CLANG
  #pragma clang diagnostic pop
#endif
   /* Set up the callbacks (only update_tips for now) */
   fetch_opts.callbacks.update_tips = &update_tips_cb;
   fetch_opts.callbacks.sideband_progress = &fetch_sideband_progress_cb;
   fetch_opts.callbacks.transfer_progress = transfer_progress_cb;


   fetch_opts.callbacks.payload=&payload;
   //fetch_opts.download_tags=GIT_REMOTE_DOWNLOAD_TAGS_AUTO;
	fetch_opts.download_tags=GIT_REMOTE_DOWNLOAD_TAGS_ALL;
   fetch_opts.update_fetchhead=1;
   //fetch_opts.callbacks.credentials = cred_acquire_cb;

   /**
    * Perform the fetch with the configured refspecs from the
    * config. Update the reflog for the updated references with
    * "fetch".
    */
   char * refspecs_arr[2];
   git_strarray myrefspecs;
   myrefspecs.strings=(char**)refspecs_arr;
   refspecs_arr[0]=(char*)newrefspec;
   myrefspecs.count=1;

   set_mark_dynamic_trace(trace);
   debug("git_remote_fetch\n");
   if (git_remote_fetch(remote, &myrefspecs, &fetch_opts, "fetch") < 0)
   {
      error("error on fetch\n");
      goto cleanup;
   }
   debug("git_remote_fetch get stats\n");

   /**
    * If there are local objects (we got a thin pack), then tell
    * the user how many objects we saved from having to cross the
    * network.
    */
   stats = git_remote_stats(remote);
   if (stats->local_objects > 0) {
      Write_dyn_trace(trace, none, "    Received %d/%d objects in %zu bytes (used %d local objects)\n",
                      stats->indexed_objects, stats->total_objects, stats->received_bytes, stats->local_objects);
   } else{
      Write_dyn_trace(trace, none, "    Received %d/%d objects in %zu bytes\n",
                      stats->indexed_objects, stats->total_objects, stats->received_bytes);
   }
   update_details(trace);
	remote_objects=stats->indexed_objects;
	
   git_checkout_options ck_opt=GIT_CHECKOUT_OPTIONS_INIT;
   ck_opt.checkout_strategy=GIT_CHECKOUT_FORCE;

   ck_opt.notify_cb=NULL;
   ck_opt.notify_flags=GIT_CHECKOUT_NOTIFY_NONE;
   ck_opt.notify_payload=NULL; // trace struct
   ck_opt.progress_cb=checkout_progress;

   ck_opt.progress_payload=&payload; // trace struct



   ck_opt.perfdata_cb=NULL;
   ck_opt.perfdata_payload=NULL; //trace struct

   //git_repository_set_head(repo, "refs/remotes/origin/test");
   Write_dyn_trace(trace, yellow, "  + Checkout %s %s\n",ref_type,ref);
   update_details(trace);

   set_mark_dynamic_trace(trace);
   if (!foundrefspec)
   {
      debug("git reset --hard\n");
      // switch to correct branch
      // git chechout ref

      git_repository_set_head(repo, newhead_ref);
      payload.msg="Files ";
      payload.msg_param="";
      git_checkout_head(repo,&ck_opt );

      // git reset --hard CI_COMMIT_SH
      Write_dyn_trace(trace, yellow, "  + git reset --hard  #%s\n",short_sha);
      set_mark_dynamic_trace(trace);
      payload.msg="Files ";
      payload.msg_param="";

      git_oid oid_git;
      result=git_oid_fromstrn(&oid_git, sha, strlen(sha));
      struct git_commit * commit;
      int result = git_commit_lookup(&commit, repo, &oid_git);
      if (result>=0)
      {
         result=git_reset(repo,(git_object*) commit,GIT_RESET_HARD , &ck_opt);
      }
      else
      {
         error("Problem create oid\n");
      }
      if (result<0)
      {
         error("problem git reset\n");
      }
      else git_object_free((git_object*) commit);
   }
   else
   {
	   
      // git chechout ref
      debug("git checkout -f \n");
      payload.msg="Files ";
      payload.msg_param="";
      git_repository_set_head(repo, newhead_ref);
      git_checkout_head(repo,&ck_opt );
   }
   update_details(trace);
cleanup:
   git_remote_free(remote);
	if (!result) return remote_objects;
	else return -1;
}

//Entry point for this command

int lgit2_describe(cJSON * job,struct trace_Struct*trace)
{
	const char * CI_PROJECT_DIR=cJSON_get_key(job,"CI_PROJECT_DIR");	int result;
	git_repository *repo=NULL;
	Write_dyn_trace(trace,bold_yellow,"\n* Git describe: ");
	update_details(trace);
	
	cJSON * env_vars=cJSON_GetObjectItem(job, "env_vars");
	const char * commit_tag=cJSON_get_key(env_vars, "CI_COMMIT_TAG");
	
	if (commit_tag)
	{   // no point in calculating if we have a git tag commit
		cJSON_AddStringToObject(env_vars, "OC_GIT_VERSION", commit_tag);
		cJSON_AddStringToObject(env_vars, "OC_GITVERSION", commit_tag);
		Write_dyn_trace(trace,cyan,"%s\n",commit_tag);
	}
	else
	{
		git_libgit2_init();
		result=git_repository_open(&repo, CI_PROJECT_DIR);
		
		if (result)
		{
			const git_error *e = giterr_last();
			printf("Error %d/%d: %s\n", result, e->klass, e->message);
			return -1;
		}
			
		git_describe_options describe_options;
		git_describe_format_options format_options;
		
		git_describe_init_options(&describe_options, GIT_DESCRIBE_OPTIONS_VERSION);
		git_describe_init_format_options(&format_options, GIT_DESCRIBE_FORMAT_OPTIONS_VERSION);
		//--tags --always --abbrev=8
		
		
		describe_options.describe_strategy = GIT_DESCRIBE_TAGS;
		describe_options.show_commit_oid_as_fallback = 1;
		format_options.abbreviated_size=8;
	 
		git_describe_result *describe_result;
		git_buf buf = { 0 };
		
		result=git_describe_workdir(&describe_result, repo, &describe_options);
		if (!result)
		{
		
			result=git_describe_format(&buf, describe_result, &format_options);
		
			if (!result)
			{
				// remove posible "\n"
				char *p=strchr(buf.ptr,'\n');
				if (p) p[0]=0;
				// add to environment as OC_GIT_VERSION
				cJSON_AddStringToObject(env_vars, "OC_GIT_VERSION", buf.ptr);
				cJSON_AddStringToObject(env_vars, "OC_GITVERSION", buf.ptr);
				Write_dyn_trace(trace,cyan,"%s\n",buf.ptr);
			}
		}
		else
		{
			const git_error *e = giterr_last();
			printf("Error %d/%d: %s\n", result, e->klass, e->message);
		}
		if (result) Write_dyn_trace(trace,magenta,"-----\n");
		update_details(trace);
		git_repository_free(repo);
	}
	return 0;
}

int lgit2_checkout(cJSON * job,struct trace_Struct*trace, int * remote_objects_update)
{
   // deleted branch, once deployed, never merged, not in master
   //char * ref="master";
   //char * sha="153be0373861fb580a138a7bc67557b6774c2b30";
   cJSON * git_info=cJSON_GetObjectItem(job, "git_info");
   //cJSON * job_info=cJSON_GetObjectItem(job, "job_info");
   const char * CI_PROJECT_DIR=cJSON_get_key(job,"CI_PROJECT_DIR");

   const char * ref=cJSON_get_key(git_info, "ref");
   const char * sha=cJSON_get_key(git_info, "sha");
   const char * repo_url=cJSON_get_key(git_info,"repo_url");
   const char * ref_type=cJSON_get_key(git_info,"ref_type");
   char * git_link=NULL;
	char * cache_dir=NULL;
	cJSON * links=cJSON_GetObjectItem(job, "layers_links");
	if (links)
	{
		debug("git checkout links:\n");
		print_json(links);
		cJSON * slink=NULL;
		cJSON_ArrayForEach(slink,links )
		{
			const char * cache_name=cJSON_get_key(slink, "name");
			const char * cache_path=cJSON_get_key(slink, "path");
			const char * cache_location=cJSON_get_key(slink, "location");
			if (cache_name && strcmp(cache_name,"git_cache")==0 &&cache_location)
			{
				asprintf(&git_link,"%s/%s",CI_PROJECT_DIR,cache_path);
				if (cache_location)
				{
					vmkdir("%s",cache_location);
					asprintf(&cache_dir, "%s",cache_location);
				}
				else
				{
					vmkdir("%s/../%s",CI_PROJECT_DIR,cache_name);
					asprintf(&cache_dir, "%s/../%s",CI_PROJECT_DIR,cache_name);
				}
				debug("cache_dir=%s\n",cache_dir);
				debug("git_link=%s\n",git_link);
				break;
			}
		}
	}
  // char * project_name=cJSON_get_key(job_info,"project_name");
   int result;
   git_repository *repo=NULL;
   Write_dyn_trace(trace,bold_yellow,"\n* Git checkout %s %s, #%.8s...\n",ref_type,ref,sha);
   update_details(trace);
  	// Before running the actual command, create an instance of the local
  	// repository and pass it to the function.
  // _rmdir("",CI_PROJECT_DIR);
   //_mkdir("",CI_PROJECT_DIR );
   debug("init repo git link=%s\n",git_link);
	//if (git_link)
	{
		debug("gitlink: %s\n",git_link);
		git_repository_init_options initopts = GIT_REPOSITORY_INIT_OPTIONS_INIT;
		initopts.flags = GIT_REPOSITORY_INIT_MKPATH;
		initopts.flags = GIT_REPOSITORY_INIT_MKDIR;
		initopts.workdir_path = CI_PROJECT_DIR;
		if (cache_dir)
		{
			result=git_repository_init_ext(&repo, cache_dir, &initopts);
			if (!result) result = git_repository_open(&repo, cache_dir);
			else debug("error git init_ext",result);
		}
		else
		{
				result=git_repository_init_ext(&repo, CI_PROJECT_DIR, &initopts);
				if (!result)
				{
				result = git_repository_open(&repo, CI_PROJECT_DIR);
				}
				else debug("error git init_ext",result);
		}
	}
	if (git_link) free(git_link);
	git_link=NULL;
	//else
	/*
	{
		result=git_repository_init(&repo,CI_PROJECT_DIR,0);
		if (result>=0)
		{
			char git_dir[1024];
			sprintf(git_dir,"%s/.git",CI_PROJECT_DIR);
			result = git_repository_open(&repo, git_dir);
		}
	}
	*/
	if (result>=0 && repo)
	{
		result = use_remote(repo, repo_url,ref,ref_type,sha,trace);
		if (result>-1)
		{
            if (remote_objects_update) * remote_objects_update=result;
			result =0;
		}
	}
	else
		printf("no repo\n");

	
   git_repository_free(repo);
	
	if (cache_dir)
	{   // create .git file with 'git-dir: <path to git dir>'
		char * tmp=NULL;
		asprintf(&tmp, "gitdir: %s/.git\n",cache_dir);
		write_file_v(tmp, strlen(tmp), "%s/.git",CI_PROJECT_DIR);
		free(tmp);
		free(cache_dir);
	}
   return result;
}

int untracked(char * projectdir, cJSON ** filelist)
{
  if (!filelist || !projectdir) return -1;
  char repodir[1024];
  sprintf(repodir,"%s",projectdir);
   char full_file_path[1024];


  debug("\n**** Untracked ***\n");
  git_repository *repo = NULL;
  if(git_repository_open_ext(&repo, repodir, 0, NULL)<0)
  {
   error("could not open repo for untracked status list\n");
   return -1;
  }
  if (git_repository_is_bare(repo))
  {
    error("Cannot report status on bare repository: %s",git_repository_path(repo));
    git_repository_free(repo);
    return -1;
  }
  git_status_options statusopt=GIT_STATUS_OPTIONS_INIT;
  statusopt.show  = GIT_STATUS_SHOW_INDEX_AND_WORKDIR;
  statusopt.flags = GIT_STATUS_OPT_INCLUDE_UNTRACKED |
    GIT_STATUS_OPT_RENAMES_HEAD_TO_INDEX |
    GIT_STATUS_OPT_SORT_CASE_SENSITIVELY | GIT_STATUS_OPT_RECURSE_UNTRACKED_DIRS;

  git_status_list *status;
  if(git_status_list_new(&status, repo, &statusopt) < 0)
  {
   error("Could not get status for untracked files\n");
   git_repository_free(repo);
   return -1;
  }
  size_t i, maxi = git_status_list_entrycount(status);
  const git_status_entry *s;


  for (i = 0; i < maxi; ++i)
  {
      s = git_status_byindex(status, i);
      if (s->status == GIT_STATUS_WT_NEW)
      {
        if (!*filelist) *filelist=cJSON_CreateArray();
         snprintf(full_file_path,1024,"%s/%s",projectdir,s->index_to_workdir->old_file.path);
        cJSON_AddItemToArray(*filelist,cJSON_CreateString(full_file_path));
        debug("#\t%s\n", s->index_to_workdir->old_file.path);
      }
  }
    debug("\n**** End Untracked ***\n");
    git_status_list_free(status);
    git_repository_free(repo);
    return 0;
}
