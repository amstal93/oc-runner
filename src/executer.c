/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! @file executer.c
 *  @brief main of oc_executer
 *  @author danny@gioxa.com
 *  @date 15/10/17
 *  @copyright (c) 2017 Danny Goossen,Gioxa Ltd.
 */

#include "deployd.h"
#include "exec.h"


/*! \var parameters
 *  \brief global settings parameters
 *  \note defined here so signal handler can access it.
 */
static parameter_t * parameters=NULL;

/*! \fn static void signal_handler(sig)
 *  \brief signal handler for gracefull shutdown
 *  \param sig signal
 */
static void signal_handler(int sig);

/*! \fn static void init_signals(void)
 * \brief initialize signal redirection
 */
static void init_signals(void);

/*! \fn static void show_usage( int exitcode)
 *  \brief print the program usage on the screen
 *  \param exitcode program exitcode
 */
static void show_usage( int exitcode);

/*! \fn static int process_options(int argc, char **argv, parameter_t *parameter)
 *  \brief process program options into parameters
 *  \param argc parameter count
 *  \param argv program arguments
 *  \param parameter global parameter sttings of dispatcher
 *  \return -1 on errors
 */
static int process_options(int argc, char **argv, parameter_t *parameter);

/*! \fn static void set_missing_parms(parameter_t *parameter)
 *  \brief set's missing parameters to default values
 *  \param parameter dispatcher settings
 */
static void set_missing_parms(parameter_t *parameter);


// ********************************************************************
//          MAIN
// ********************************************************************

/*! \fn int main(int argc, char ** argv)
    \brief main oc_executer
 */
int main(int argc, char ** argv, char ** envp)
{
	

   parameters= calloc(sizeof(parameter_t),1);
   if ((optind=process_options(argc, argv,parameters)) < 0)
   {
      error(" *** ERRORin command line options, exit \n");
      return (-1);
   }
   if (argc > 1 && optind < argc) {
      alert("ignoring non option arguments\n");
   }
	if (parameters->pipe_env)
	{
		cJSON * env_json= cJSON_Create_env_obj(envp);
		char * envjsonstr=cJSON_Print(env_json);
		
		FILE * pipe=fdopen((parameters->pipe_env-1), "w");
		fwrite(envjsonstr, 1, strlen(envjsonstr), pipe);
		fclose(pipe);
		exit (parameters->exit);
	}
	if (parameters->pipe_pwd)
	{
		char * envjsonstr=NULL;
		char path[PATH_MAX];
		asprintf(&envjsonstr, "{ \"PWD\":\t\"%s\" }\n",getcwd(path, PATH_MAX));
		FILE * pipe=fdopen((parameters->pipe_pwd-1), "w");
		fwrite(envjsonstr, 1, strlen(envjsonstr), pipe);
		fclose(pipe);
		exit (parameters->exit);
	}
	if (argv[0][0]=='/')
	{
		asprintf(&parameters->commandpath, "%s",argv[0]);
	}
	else
	{
	char path[PATH_MAX];
	asprintf(&parameters->commandpath, "%s/%s",getcwd(path, PATH_MAX),argv[0]);
	}
	printf("Command_path=%s\n",parameters->commandpath);
   set_missing_parms(parameters);
   
   if (parameters->verbose) setverbose();
   if (parameters->debug) setdebug();
	
	cJSON * job=NULL;
	
	const char* job_ser_env=getenv(JOBENVIRONMENTVAR);
	
	if (job_ser_env)
	{
		alert("got %s env\n",JOBENVIRONMENTVAR);
		job=cJSON_Parse(job_ser_env);
		
	}
	if (!job)
	{
		alert("No Job, bit of sleep, then exit\n");
		usleep(1000000);
		alert("exit after sleep\n");
		exit(1);
	}
	alert("got job:\n");
	unsetenv(JOBENVIRONMENTVAR);
 
    if (getuid()==0)
    {
       alert("running as root, not recommended!\n");
    }
   else
   {
       alert("running as non-root, id: %d!\n",getuid());
   }

   init_signals();
   thread_setup(); //TODO move this to main
   curl_global_init(CURL_GLOBAL_SSL); //TODO move this to main
   git_libgit2_init();
   git_libgit2_opts(GIT_OPT_SET_SSL_CERT_LOCATIONS, DEFAULT_CA_BUNDLE, NULL);
   git_libgit2_opts(GIT_OPT_ENABLE_OFS_DELTA, 1);
	
	//git_threads_init();
   // check for jobs and execute!!! yah
   
   parameters->shell =check_shell();
	
	do_job(job, parameters);
   
   curl_global_cleanup(); //TODO move this to main
   thread_cleanup(); //TODO move this to main
   git_libgit2_shutdown();
   alert("exit oc-executer\n");
   // TODO free parameters!!!
   return 0;
}
/*------------------------------------------------------------------------
 *
 * Signal handler
 *
 *------------------------------------------------------------------------*/

static void signal_handler(sig)
int sig;
{
   alert ("\nEXIT: Signal %d : %s !!!\n",sig,strsignal(sig));
   switch(sig) {
      case SIGHUP:
      {
         parameters->hubsignal=1;
         debug ("set parameters->hubsignal\n ");
         break;
      }
      case SIGSEGV:
      {
         parameters->exitsignal=1;
         debug ("set parameters->exitsignal\n ");
         usleep(3000000);
         exit(-1);
      }
      case SIGSTOP:
      case SIGQUIT:
      case SIGINT:
      case SIGTERM:
      {
         parameters->exitsignal=1;
         debug ("set parameters->exitsignal\n ");
         break;
      }
   }
}

/*------------------------------------------------------------------------
 *
 * initialize the signal redirections
 *
 *------------------------------------------------------------------------*/

void init_signals()
{
   signal(SIGHUP,&signal_handler); /* catch hangup signal */
   signal(SIGTERM,&signal_handler); /* catch kill signal */
   signal(SIGINT,&signal_handler); /* catch kill signal */
   signal(SIGSTOP,&signal_handler); /* catch kill signal */
   signal(SIGQUIT,&signal_handler); /* catch kill signal */
   signal(SIGSEGV,&signal_handler); /* catch kill signal */
   
}

void show_usage( int exitcode)
{
   error("Usage: oc-executer [--options]\n\n");
   error("     --options=value:\n");
   error("version        : Shows the version\n");
   error("help           : Shows the version and usage\n");
   error("verbose        : turns on verbose output mode\n");
   error("quiet          : quiet down output\n");
   error("debug          : debug output\n");
   error("\n");
   error("Defaults: verbose    = %d\n",   VERBOSE_YN);
   error("\n");
   exit(exitcode);
}

/*------------------------------------------------------------------------
 * int process_options(int argc, char **argv, tb_parameter_t *parameter)
 *
 * Process the program line options and set the parameters accordingly
 *------------------------------------------------------------------------*/
int process_options(int argc, char **argv, parameter_t *parameter)
{
   
   struct option long_options[] = {
      { "verbose",    0, NULL, 'v' },
      { "quiet",      0, NULL, 'q' },
      { "help",       0, NULL, 'j' },
      { "version",    0, NULL, 'V' },
      { "debug",      0, NULL, 'D' },
      { "config",         1, NULL, 'c' },
      { "prefix"     ,1, NULL, 'p' },
      { "timeout"    ,1, NULL, 't' },
	  { "pipe_env"    ,1, NULL, 'P' },
	   { "pipe_pwd"    ,1, NULL, 'Q' },
	   	{ "exit"    ,1, NULL, 'E' },
      { NULL,         0, NULL, 0 } };
   
   int           which;
   optind=1;  // reset if called again, needed for fullcheck as we call twice (server and main)
   // ref : http://man7.org/linux/man-pages/man3/getopt.3.html
   if (argc>1)
   {
      /* for each option found */
      while ((which = getopt_long(argc, argv, "+", long_options, NULL)) > 0) {
         
         /* depending on which option we got */
         switch (which) {
               /* --verbose    : enter verbose mode for debugging */
            case 'v':
            {
               if (parameter->quiet)
               {
                  error("Invalid option, choose quiet or verbose\n");
                  return -1;
               }
               else
               {
                  parameter->verbose = 1;
               }
            }
               break;
            case 'q':
            {
               if (parameter->verbose)
               {
                  error("Invalid option, choose quiet or verbose\n");
                  return -1;
               }
               else
                  parameter->quiet = 1;
            }
               break;
			 case 'P':
			 {
					parameter->pipe_env=atoi(optarg)+1; ;
			 }
				break;
			 case 'Q':
			 {
					parameter->pipe_pwd=atoi(optarg)+1; ;
			 }
					break;
			 case 'E':
			 {
					parameter->exit=atoi(optarg) ;
			 }
					break;
             case 'p': {
               parameter->prefix =malloc(strlen(optarg)+1);
               sprintf(parameter->prefix,"%s",optarg);
            }
               break;
            case 't': {
               parameter->timeout   = atoi(optarg);
            }
               break;
            case 'D': {
               parameter->debug = 1;
               break;
            }
            case 'c': {
               parameter->config_file=calloc (strlen(optarg)+1,1);
               sprintf(parameter->config_file,"%s",optarg);
            }
               break;
            case 'V':
            {
               error("%s\n",GITVERSION);
               exit(0);
            }
            case 'j': {
               error("\ndeloyd Git Version: %s\n \n",GITVERSION);
               show_usage(0);
            }
               /* otherwise    : display usage information */
            default:
               ;
               show_usage(1);
         }
      }
   }
   // if NULL then no option was given and we default define the dir where tblastd was started !!!!!!
   //value = getenv(name);
   return optind;
}

/*------------------------------------------------------------------------
 * void rset_missing_parms(tb_parameter_t *parameter)
 *
 * set the parameters to default if not in commandline or config
 *------------------------------------------------------------------------*/
void set_missing_parms(parameter_t *parameter)
{
   parameter->hubsignal=0;
   parameter->exitsignal=0;
   if (parameter->config_file == NULL)
   {
      parameter->config_file=calloc(strlen(CONFIG_FILE)+1,1);
      sprintf(parameter->config_file,"%s",CONFIG_FILE);
   }
   if (parameter->prefix == NULL)
   {
      parameter->prefix=calloc(strlen(PREFIX)+1,1);
      sprintf(parameter->prefix,"%s",PREFIX);
   }
   
   if (parameter->timeout==0) parameter->timeout  = TIME_OUT_CHILD;
   
   if (! parameter->verbose && !parameter->quiet) parameter->verbose = VERBOSE_YN;
   
   if (parameter->ca_bundle == NULL)
   {
      
      parameter->ca_bundle=calloc(strlen(DEFAULT_CA_BUNDLE)+1,1);
      sprintf(parameter->ca_bundle,"%s",DEFAULT_CA_BUNDLE);
   }
   parameter->testprefix=calloc(strlen("")+1,1);
   sprintf(parameter->testprefix,"%s","");
   
   return;
}




