/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! \file Simple_http.h
 *  \brief Header file for simple http requests
 *  \author Created by Danny Goossen on 23/7/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 *
 */
#ifndef __oc_runner__Simple_http__
#define __oc_runner__Simple_http__

#include <curl/curl.h>

/** Enumeration for easy http */
enum easy_http_verbose{
	/** quiet curl request */
	easy_http_quiet=0,
	/** verbose curl request */
	easy_http_verbose=1,
};

/** Enumeration for easy http */
enum easy_http_folow_location{
	/** quiet curl request */
	easy_http_dont_follow=0,
	/** verbose curl request */
	easy_http_follow_location=1,
};

/** Enumeration for easy http SSL */
enum easy_http_SSL{
	/** don't verify SSL peer*/
	easy_http_NO_SSL_VERIFYPEER=0,
	/** veryfy SSL peer */
	easy_http_SSL_VERIFYPEERL=1,
};



/**
 \brief Simple http init
 */
CURL * simple_http_init(const char * CAINFO,enum easy_http_folow_location FOLLOWLOCATION,enum easy_http_verbose VERBOSE,enum easy_http_SSL SSL_VERIFYPEER, const char * USERAGENT);



/**
 \brief Simple http cleanup
 */
void simple_http_clean(CURL ** curlp);


/**
 \brief Simple http GET
 */
int simple_httpget_v(CURL * curl,char**wresult,size_t*len_in,const char *url,...);

/**
 \brief Simple http POST
 */
int simple_httppost_v(CURL * curl,char**wresult,size_t * len_in, const char *url, ...);

/**
 \brief Customn http
 */
int custom_http_v(CURL * curl,const char *data_out,size_t len_out,const cJSON * headers_out,char**data_in,size_t * len_in,cJSON ** headers_in,const char * methode,const char *url,...);

/**
 \brief print http or curl error to stderr
 \param error http or curl code
 */

void print_api_error_stderr(int error);

#endif /* defined(__oc_runner__Simple_http__) */
