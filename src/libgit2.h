//
//  libgit2.h
//  deployctl
//
//  Created by Danny Goossen on 19/8/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deployctl__libgit2__
#define __deployctl__libgit2__

#include "common.h"
int lgit2_checkout(cJSON * job,struct trace_Struct*trace, int * remote_objects_update);
int untracked(char * projectdir, cJSON ** filelist);
int lgit2_describe(cJSON * job,struct trace_Struct*trace);
#endif /* defined(__deployctl__libgit2__) */
