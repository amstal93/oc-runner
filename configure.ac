#  configure.ac
#
# Created by Danny Goossen on 2/3/16.
#
# MIT License
#
# Copyright (c) 2017 deployctl, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


AC_PREREQ([2.69])

AC_INIT([oc-runner],
	m4_esyscmd([build-aux/git-version-gen --prefix "" .tarball-version]),
	[https://gitlab.gioxa.com/deployctl/oc-runner/issues])


#AC_CONFIG_SRCDIR([src/common.h])
#AC_CONFIG_HEADERS([config.h])

AC_CONFIG_MACRO_DIR([m4])

# place to put some extra build scripts installed
AC_CONFIG_AUX_DIR([build-aux])

# check the cross compile triplets etc
AC_CANONICAL_SYSTEM

# New AM_INIT, add -Werror, will fail when adding posic vars with the automake
AM_INIT_AUTOMAKE([ 1.11 subdir-objects tar-pax ])

AC_LANG([C])

# Checks for programs.
AC_PROG_CC_C99
AC_PROG_LN_S
AM_PROG_CC_C_O

AC_CHECK_TOOL(kill, kill)
AC_CHECK_TOOL(xargs, xargs)
AC_CHECK_TOOL(cat, cat)


# Version Control
GIT_VERSION=$(git describe --abbrev=8 --always --tags)

AC_SUBST(GIT_VERSION)
AC_SUBST(VERSION)

# -lutil -lgit2 -lrt -lpthread -lcjson -lcrypto -lssl -lzip -lcurl -lm 

# Checks for libraries.
AC_CHECK_LIB([m], [main])
AC_CHECK_LIB([cjson], [main])
AC_CHECK_LIB([yaml], [main])
#PKG_CHECK_MODULES([cJSON], [libcjson], [true], [true])
AC_CHECK_LIB([z], [main])
AC_CHECK_LIB([rt], [main])
AC_CHECK_LIB([zip],[main])
AC_CHECK_LIB([util], [main])
AC_CHECK_LIB([pthread],[main])
AC_CHECK_LIB([cares],[main])
#AC_CHECK_LIB([syslog],[syslog])
AC_CHECK_LIB([crypto],[main])
AC_CHECK_LIB([ssl],[main])
AC_CHECK_LIB([http_parser],[main])
AC_CHECK_LIB([curl], [main])
AC_CHECK_LIB([git2], [main])
AC_CHECK_LIB([bz2], [main])
#AC_CHECK_LIB([tar-root], [main])
AC_CHECK_LIB([archive], [main])

saved_LIBS=$LIBS

PKG_CHECK_MODULES([CHECK], [check >= 0.10.0], [true], [true])

LIBS=$saved_LIBS

AC_SUBST([AM_CFLAGS], [-Wall])

AX_VALGRIND_DFLT([memcheck], [on])
AX_VALGRIND_DFLT([sgcheck], [off])
AX_VALGRIND_CHECK

AX_CODE_COVERAGE

#ac_cv_func_malloc_0_nonnull=yes


# Checks for header files.
##### Check for Fundamental EC #####
AC_CHECK_HEADERS([openssl/fec.h])

AC_CHECK_HEADERS([arpa/inet.h fcntl.h float.h limits.h netdb.h netinet/in.h stddef.h stdint.h stdlib.h string.h strings.h sys/file.h sys/socket.h syslog.h unistd.h curl/curl.h  cJSON.h check.h zip.h openssl/err.h ftw.h] pthread.h  pty.h ares.h)

# Checks for typedefs, structures, and compiler characteristics.
AC_CHECK_HEADER_STDBOOL
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_TYPE_SSIZE_T
AC_TYPE_UID_T


# Checks for library functions.
AC_FUNC_ERROR_AT_LINE
AC_FUNC_FORK
AC_FUNC_MALLOC
AC_CHECK_FUNCS([bzero floor memset pow socket strchr strerror])
AC_CHECK_FUNCS([dup2])
AC_CHECK_FUNCS([regcomp])
AC_CHECK_FUNCS([pipe2])
AC_CHECK_FUNCS([select])
AC_CHECK_FUNCS([strstr])
AC_CHECK_FUNCS([strndup])
AC_CHECK_FUNCS([nftw])
AC_CHECK_FUNCS([memrchr])
AC_CHECK_FUNCS([asprintf])
AC_CHECK_FUNCS([pthread_cancel])

AC_FUNC_REALLOC


AC_CONFIG_FILES([Makefile
                 tools/Makefile
                 src/Makefile
                 tests/Makefile
                 ])
AC_OUTPUT

AC_MSG_NOTICE([
  Configuration Summary
  ---------------------
  Package                                   : ${PACKAGE_NAME}
  Version                                   : ${PACKAGE_VERSION}
  Interface                                 : ${LIBOPENTHREAD_VERSION_INFO//:/.}
  Build system                              : ${build}
  Host system                               : ${host}
  Target system                             : ${target}
  Target architecture                       : ${target_cpu}
  Target OS                                 : ${target_os}
  Cross compiling                           : ${cross_compiling}
  Build tools                               : ${build_tools}
  Prefix                                    : ${prefix}
  Documentation support                     : ${nl_cv_build_docs}
  C Preprocessor                            : ${CPP}
  C Compiler                                : ${CC}
  C++ Preprocessor                          : ${CXXCPP}
  C++ Compiler                              : ${CXX}
  Archiver                                  : ${AR}
  Archive Indexer                           : ${RANLIB}
  Symbol Stripper                           : ${STRIP}
  Object Copier                             : ${OBJCOPY}
  C Preprocessor flags                      : ${CPPFLAGS:--}
  C Compile flags                           : ${CFLAGS:--}
  C++ Compile flags                         : ${CXXFLAGS:--}
  Link flags                                : ${LDFLAGS:--}
  Link libraries                            : ${LIBS}
  Link libraries for check (CHECK_LIBS)     : ${CHECK_LIBS}
  Check C-FLAGS                             : ${CHECK_CFLAGS}
  LDEXTRA Flags                             : ${LDEXTRA_Flags:--}
  LIBTOOL_DEPS                              : ${LIBTOOL_DEPS}
  LD                                        : ${LD}
  EXTRA_LDFLAGS                             : ${EXTRA_LDFLAGS}
  EXTRA_CFLAGS                              : ${EXTRA_CFLAGS}
  MY_OBJCOPY                                : ${MY_OBJCOPY}
  O_EXTRA_LD                                : ${O_EXTRA_LD}
  CC_FOR_BUILD                              : ${CC_FOR_BUILD}
  DEPLOY_CONF_DIR              : ${deployconfigdir}
  DEPLOY_HTML_ERROR_DIR        : ${deployhtmlerrdir}
  systemd_unit_DIR             : ${systemdunitdir}
])
